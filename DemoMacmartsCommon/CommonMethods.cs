﻿using OfficeOpenXml;
using System;
using System.Data;
using System.Text;
using System.Web;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using OfficeOpenXml.Style;
using System.Security.Cryptography;
using System.Configuration;
using System.Net;
using System.Collections.Generic;
using System.Reflection;
using System.Data.OleDb;
using System.Linq;

namespace DemoMacmartsCommon
{
    public class CommonMethods
    {
        string FTP = Convert.ToString(ConfigurationManager.AppSettings["ftp"]);
        string FTPFolder = Convert.ToString(ConfigurationManager.AppSettings["ftpFolder"]);
        string FTPUserName = Convert.ToString(ConfigurationManager.AppSettings["ftpUserName"]);
        string FTPPassword = Convert.ToString(ConfigurationManager.AppSettings["ftpPassword"]);

        public static void ExportToExcel(DataTable dt, string strSheetName = "Export")
        {
            if (dt.Rows.Count > 0)
            {
                try
                {
                    using (ExcelPackage pck = new ExcelPackage())
                    {
                        //Create the worksheet
                        ExcelWorksheet ws = pck.Workbook.Worksheets.Add(strSheetName);

                        //Load the datatable into the sheet, starting from cell A1. Print the column names on row 1
                        ws.Cells["A1"].LoadFromDataTable(dt, true);

                        //Format the header for column 1-3

                        string strLastCol = "A1:" + GetExcelColumnName(dt.Columns.Count) + "1";
                        using (ExcelRange rng = ws.Cells[strLastCol])
                        {
                            rng.Style.Font.Bold = true;
                            rng.Style.Fill.PatternType = ExcelFillStyle.Solid;                      //Set Pattern for the background to Solid
                            rng.Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.FromArgb(79, 129, 189));  //Set color to dark blue
                            rng.Style.Font.Color.SetColor(System.Drawing.Color.White);
                        }

                        using (var range = ws.Cells[2, 1, dt.Rows.Count + 2, dt.Columns.Count])
                        {

                            range.AutoFitColumns();
                        }



                        //Write it back to the client
                        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + strSheetName + ".xlsx");
                        HttpContext.Current.Response.BinaryWrite(pck.GetAsByteArray());
                        HttpContext.Current.Response.End();
                    }
                }
                catch (Exception ex)
                {
                    //lblMessage.Visible = false;
                    //if (ex.Message != "Thread was being aborted.")
                    //{
                    //    Response.Write(ex.Message);
                    //}
                }
            }
            else
            {
                //lblMessage.Text = MessageConstants.Admin_NoRecordFoundMsg;
                //lblMessage.Visible = true;
            }
        }
        private static string GetExcelColumnName(int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return columnName;
        }

        //public void ExportListUsingEPPlus(IEnumerable<T> t)
        //{
        //   var data=t;

        //    ExcelPackage excel = new ExcelPackage();
        //    var workSheet = excel.Workbook.Worksheets.Add("Sheet1");
        //    workSheet.Cells[1, 1].LoadFromCollection(data, true);
        //    using (var memoryStream = new MemoryStream())
        //    {
        //        HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
        //        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;  filename=Contact.xlsx");
        //        excel.SaveAs(memoryStream);
        //        memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
        //        HttpContext.Current.Response.Flush();
        //        HttpContext.Current.Response.End();
        //    }
        //}

        /// <summary>
        /// This Mehtod is generic to export DATATALE INTO PDF TABLE format
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="pdfFileName"></param>
        public static void ExportToPDF(DataTable dataTable, string pdfFileName = "Export PDF")
        {
            Document document = new Document(PageSize.A4, 10, 10, 90, 10);
            System.IO.MemoryStream mStream = new System.IO.MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(document, mStream);
            int cols = dataTable.Columns.Count;
            int rows = dataTable.Rows.Count;

            // iTextSharp.text.Font _headerFont = FontFactory.GetFont(FontFactory.HELVETICA,12, iTextSharp.text.Font.BOLD);

            /*iTextSharp.text.Font _footerFont = FontFactory.GetFont(FontFactory.HELVETICA, 10, iTextSharp.text.Font.BOLD);
            //document.Header = new HeaderFooter(new Phrase(pdfFileName, _headerFont), false);
            document.Footer = new HeaderFooter(new Phrase("© Macnovate India Automation Services Pvt Limited", _footerFont), false);

            document.Open();
            Guid guid = Guid.NewGuid();

            pdfFileName = string.Concat(pdfFileName, "_", DateTime.Now.ToString("dd-MMM-yyyy"), "_", guid, ".pdf");

            // pdfTable create and set
            iTextSharp.text.Table pdfTable = new iTextSharp.text.Table(cols, rows);
            
            pdfTable.BorderWidth = 1;
            pdfTable.Width = 100;
            pdfTable.Padding = 1;
            pdfTable.Spacing = 1;

            //table headers
            for (int i = 0; i < cols; i++)
            {
                Cell cellCols = new Cell();
                iTextSharp.text.Font ColFont = FontFactory.GetFont(FontFactory.HELVETICA, 8, iTextSharp.text.Font.NORMAL);
                Chunk chunkCols = new Chunk(dataTable.Columns[i].ColumnName, ColFont);
                cellCols.Add(chunkCols);
                pdfTable.AddCell(cellCols);

            }


            //table data 
            for (int k = 0; k < rows; k++)
            {
                for (int j = 0; j < cols; j++)
                {
                    Cell cellRows = new Cell();
                    Font RowFont = FontFactory.GetFont(FontFactory.HELVETICA, 12);
                    Chunk chunkRows = new Chunk(dataTable.Rows[k][j].ToString(), RowFont);
                    cellRows.Add(chunkRows);
                    pdfTable.AddCell(cellRows);

                }
            }

            document.Add(pdfTable);*/

            PdfPTable tableLayout = new PdfPTable(4);
            document.Open();


            document.Close();
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + pdfFileName);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.BinaryWrite(mStream.ToArray());
            HttpContext.Current.Response.End();
        }

        public static string ExportHTMLToPDF(string type, string content)
        {
            string pdfFileName = type;
            Document document = new Document();
            try
            {
                //Document document = new Document();

                Guid guid = Guid.NewGuid();
                pdfFileName = string.Concat(pdfFileName, "_", DateTime.Now.ToString("dd-MMM-yyyy"), "_", guid, ".pdf");
                //PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("//Upload/tempPDFFiles/") + pdfFileName, FileMode.Create));

                //System.IO.MemoryStream mStream = new System.IO.MemoryStream();
                //PdfWriter writer = PdfWriter.GetInstance(document, mStream);

                //document.Open();
                ////Image pdfImage = Image.GetInstance(Server.MapPath("logo.png"));

                ////pdfImage.ScaleToFit(100, 50);
                //// pdfImage.Alignment = iTextSharp.text.Image.UNDERLYING; pdfImage.SetAbsolutePosition(180, 760);
                ////document.Add(pdfImage);

                //iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
                //iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);
                //hw.Parse(new StringReader(content));
                //document.Close();
                //if (ShowPdf(pdfFileName) == "success")
                //{
                //    return pdfFileName;
                //}
                //else
                //{
                //    return "error";
                //}

                String htmlText = content;

                PdfWriter.GetInstance(document, new FileStream(HttpContext.Current.Server.MapPath("//Upload/tempPDFFiles/") + pdfFileName, FileMode.Create));
                // document.Footer=new HeaderFooter()
                document.Open();
                //iTextSharp.text.html.HtmlParser hw = new iTextSharp.text.html.HtmlParser(document);
                iTextSharp.text.html.simpleparser.HTMLWorker hw = new iTextSharp.text.html.simpleparser.HTMLWorker(document);
                hw.Parse(new StringReader(htmlText));
                document.Close();
            }
            catch { document.Close(); pdfFileName = "error"; }
            return pdfFileName;
        }
        private static string ShowPdf(string s)
        {
            try
            {
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.AddHeader("Content-Disposition", "inline;filename=" + s);
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.WriteFile(HttpContext.Current.Server.MapPath("//Upload/tempPDFFiles/") + s);
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.Clear();
                return "success";
            }
            catch (Exception err)
            {
                return "error";
            }

        }

        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(toEncrypt);

            //System.Configuration.AppSettingsReader settingsReader =
            //                                    new AppSettingsReader();
            // Get the key from config file

            //string key = (string)settingsReader.GetValue("SecurityKey",
            //                                                 typeof(String));

            string key = "Macmart_Bhushan@123";
            //System.Windows.Forms.MessageBox.Show(key);
            //If hashing use get hashcode regards to your key
            if (useHashing)
            {
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //Always release the resources and flush data
                // of the Cryptographic service provide. Best Practice

                hashmd5.Clear();
            }
            else
                keyArray = UTF8Encoding.UTF8.GetBytes(key);

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes.
            //We choose ECB(Electronic code Book)
            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)

            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateEncryptor();
            //transform the specified region of bytes array to resultArray
            byte[] resultArray =
              cTransform.TransformFinalBlock(toEncryptArray, 0,
              toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor
            tdes.Clear();
            //Return the encrypted data into unreadable string format
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }

        public static string Decrypt(string cipherString, bool useHashing)
        {
            byte[] keyArray;
            //get the byte code of the string

            byte[] toEncryptArray = Convert.FromBase64String(cipherString);

            //System.Configuration.AppSettingsReader settingsReader =
            //                                    new AppSettingsReader();
            ////Get your key from config file to open the lock!
            //string key = (string)settingsReader.GetValue("SecurityKey",
            //                                             typeof(String));
            string key = "Macmart_Bhushan@123";

            if (useHashing)
            {
                //if hashing was used get the hash code with regards to your key
                MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider();
                keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                //release any resource held by the MD5CryptoServiceProvider

                hashmd5.Clear();
            }
            else
            {
                //if hashing was not implemented get the byte code of the key
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }

            TripleDESCryptoServiceProvider tdes = new TripleDESCryptoServiceProvider();
            //set the secret key for the tripleDES algorithm
            tdes.Key = keyArray;
            //mode of operation. there are other 4 modes. 
            //We choose ECB(Electronic code Book)

            tdes.Mode = CipherMode.ECB;
            //padding mode(if any extra byte added)
            tdes.Padding = PaddingMode.PKCS7;

            ICryptoTransform cTransform = tdes.CreateDecryptor();
            byte[] resultArray = cTransform.TransformFinalBlock(
                                 toEncryptArray, 0, toEncryptArray.Length);
            //Release resources held by TripleDes Encryptor                
            tdes.Clear();
            //return the Clear decrypted TEXT
            return UTF8Encoding.UTF8.GetString(resultArray);
        }
        public static void LogError(string Pagename, string Methodname, Exception ex)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                sb.AppendLine("-----------------------------------------------------------");
                sb.AppendLine(string.Format("Pagename: {0}", Pagename));
                sb.AppendLine(string.Format("Methodname: {0}", Methodname));
                sb.AppendLine(string.Format("Message: {0}", ex.Message));
                sb.AppendLine(string.Format("StackTrace: {0}", ex.StackTrace));
                sb.AppendLine(string.Format("Source: {0}", ex.Source));
                sb.AppendLine(string.Format("Message: {0}", ex.ToString()));
                sb.AppendLine("-----------------------------------------------------------");

                string filePath = ConfigurationManager.AppSettings.Get("Errorlogfilepath");
                string path = HttpContext.Current.Server.MapPath(filePath);
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(sb.ToString());
                    writer.Close();
                }
                sb.Clear();
            }
            catch
            {
            }
        }



        public string FileUpload(HttpPostedFileBase file, string FolderName, out string _FileNameOnly)
        {
            string Result = "";
            _FileNameOnly = "";
            try
            {
                Guid guid;
                guid = Guid.NewGuid();

                string fileExtension = Path.GetExtension(file.FileName);
                FtpWebRequest request;
                string absoluteFileName = string.Concat(Path.GetFileNameWithoutExtension(file.FileName), "_", DateTime.Now.ToString("dd-MMM-yyyy"), "_" + guid, fileExtension);

                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}/{3}", FTP, FTPFolder, FolderName, absoluteFileName))) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                request.ConnectionGroupName = "group";

                Stream fs = file.InputStream;
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
                _FileNameOnly = absoluteFileName;
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUpload", err);
                Result = "error";
            }
            return Result;

        }

        public string FileUploadDocsPdf(HttpPostedFileBase file, out string _FileNameOnly)
        {
            string Result = "";
            _FileNameOnly = "";
            try
            {
                Guid guid;
                guid = Guid.NewGuid();

                string fileExtension = Path.GetExtension(file.FileName);
                FtpWebRequest request;
                string absoluteFileName = string.Concat(Path.GetFileNameWithoutExtension(file.FileName), "_", DateTime.Now.ToString("dd-MMM-yyyy"), "_" + guid, fileExtension);
                absoluteFileName = absoluteFileName.Replace("#", "");
                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}/{3}/{4}", FTP, FTPFolder, "docs", "pdf", absoluteFileName))) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                request.ConnectionGroupName = "group";

                Stream fs = file.InputStream;
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
                _FileNameOnly = absoluteFileName;
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUploadDocsPdf", err);
                Result = "error";
            }
            return Result;

        }

        public string FileUploadSnapshotOld(HttpPostedFileBase file, string FolderName, string absoluteFileName)
        {
            string Result = "";
            //_FileNameOnly = "";
            try
            {
                //Guid guid;
                //guid = Guid.NewGuid();

                string fileExtension = Path.GetExtension(file.FileName);
                FtpWebRequest request;
                //string absoluteFileName = string.Concat(Path.GetFileNameWithoutExtension(file.FileName), "_", DateTime.Now.ToString("dd-MMM-yyyy"), "_" + guid, fileExtension);


                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}/{3}/{4}/{5}", FTP, FTPFolder, "docs", "snapshot", FolderName, absoluteFileName))) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                request.ConnectionGroupName = "group";

                Stream fs = file.InputStream;
                byte[] buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
                fs.Close();

                Stream requestStream = request.GetRequestStream();
                requestStream.Write(buffer, 0, buffer.Length);
                requestStream.Flush();
                requestStream.Close();
                // _FileNameOnly = absoluteFileName;
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUploadDocsPdf", err);
                Result = "error";
            }
            return Result;

        }
        public string FileUploadSnapshot(HttpPostedFileBase fileToUpload, string FolderName, string absoluteFileName)
        {
            string Result = "";
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}/{3}/{4}/{5}", FTP, FTPFolder, "docs", "snapshot", "big", absoluteFileName)));

                request.Method = WebRequestMethods.Ftp.UploadFile;
                // This example assumes the FTP site uses anonymous logon.
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                // Copy the contents of the file to the request stream.
                StreamReader sourceStream = new StreamReader(fileToUpload.InputStream);
                byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
                sourceStream.Close();
                request.ContentLength = fileContents.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(fileContents, 0, fileContents.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUploadSnapshot", err);
                Result = "error";
            }

            return Result;
        }

        public string FileUploadSnapshotThumb(string fileName)
        {
            string Result = "";

            FtpWebRequest request;
            try
            {
                string absoluteFileName = Path.GetFileName(fileName);

                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}/{3}/{4}/{5}", FTP, FTPFolder, "docs", "snapshot", "small", absoluteFileName))) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                request.ConnectionGroupName = "group";

                using (FileStream fs = File.OpenRead(fileName))
                {
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    fs.Close();
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(buffer, 0, buffer.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUploadSnapshotThumb", err);
                Result = "error";
            }
            return Result;

        }

        public string FileUploadSnapshotThumbAndOriginal(string fileName, string FolderName)
        {
            string Result = "";

            FtpWebRequest request;
            try
            {
                string absoluteFileName = Path.GetFileName(fileName);

                request = WebRequest.Create(new Uri(string.Format(@"ftp://{0}/{1}/{2}/{3}/{4}/{5}", FTP, FTPFolder, "docs", "snapshot", FolderName, absoluteFileName))) as FtpWebRequest;
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.UseBinary = true;
                request.UsePassive = true;
                request.KeepAlive = true;
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                request.ConnectionGroupName = "group";

                using (FileStream fs = File.OpenRead(fileName))
                {
                    byte[] buffer = new byte[fs.Length];
                    fs.Read(buffer, 0, buffer.Length);
                    fs.Close();
                    Stream requestStream = request.GetRequestStream();
                    requestStream.Write(buffer, 0, buffer.Length);
                    requestStream.Flush();
                    requestStream.Close();
                }
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUploadSnapshotThumb", err);
                Result = "error";
            }
            return Result;

        }

        public string FileRemove(string Filename)
        {
            string Result = "";
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(string.Format(@"ftp://{0}/{1}/{2}/{3}/{4}", FTP, FTPFolder, "docs", "pdf", Filename));
                request.Credentials = new NetworkCredential(FTPUserName, FTPPassword);
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                response.Close();
                Result = "success";
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "FileUploadDocsPdf", err);
                Result = "error";
            }
            return Result;
        }
        public static DataSet GetDataTableFromExcel(string path, bool hasHeader = true)
        {
            DataSet ds = new DataSet();
            DataTable tbl = new DataTable();
            try
            {
                using (var pck = new ExcelPackage())
                {
                    using (var stream = File.OpenRead(path))
                    {
                        pck.Load(stream);
                    }
                    var ws = pck.Workbook.Worksheets.First();

                    foreach (var firstRowCell in ws.Cells[1, 1, 1, ws.Dimension.End.Column])
                    {
                        tbl.Columns.Add(hasHeader ? firstRowCell.Text : string.Format("Column {0}", firstRowCell.Start.Column));
                    }
                    var startRow = hasHeader ? 2 : 1;
                    for (int rowNum = startRow; rowNum <= ws.Dimension.End.Row; rowNum++)
                    {
                        var wsRow = ws.Cells[rowNum, 1, rowNum, ws.Dimension.End.Column];
                        DataRow row = tbl.Rows.Add();
                        foreach (var cell in wsRow)
                        {
                            row[cell.Start.Column - 1] = cell.Text;
                        }
                    }

                }
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "GetDataTableFromExcel", err);
            }
            ds.Tables.Add(tbl);
            return ds;
        }
        public static DataSet GetExcelData(string FilePath, string Extension)
        {


            DataSet ds = new DataSet();
            OleDbConnection connExcel = new OleDbConnection();
            OleDbCommand cmdExcel = new OleDbCommand();
            OleDbDataAdapter oda = new OleDbDataAdapter();

            string PoSheetName = Convert.ToString(ConfigurationManager.AppSettings["PoSheetName"]);

            //LogError("CommonMethods", "GetExcelData", new Exception(PoSheetName));
            try
            {
                string isHDR = "Yes";
                string conStr = "";
                switch (Extension)
                {
                    case ".xls": //Excel 97-03
                        conStr = Convert.ToString(ConfigurationManager.AppSettings["Excel03ConString"]);
                        break;
                    case ".xlsx": //Excel 07
                        conStr = Convert.ToString(ConfigurationManager.AppSettings["Excel07ConString"]);
                        break;
                }
                conStr = String.Format(conStr, FilePath, isHDR);
                connExcel.ConnectionString = conStr;
                DataTable dt = new DataTable();
                cmdExcel.Connection = connExcel;

                //Get the name of First Sheet
                connExcel.Open();
                DataTable dtExcelSchema;
                dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                //LogError("CommonMethods", "GetExcelData", new Exception("TEst3"));

                if (dtExcelSchema != null)
                {
                    if (dtExcelSchema.Rows.Count > 0)
                    {
                        var SheetList = from s in dtExcelSchema.Select().ToList()
                                        where s.Field<string>("TABLE_NAME").EndsWith("$")
                                        select s.Field<string>("TABLE_NAME");

                        //LogError("CommonMethods", "GetExcelData", new Exception("TEst4"));

                        if (SheetList.Count() > 0)
                        {
                            foreach (var SheetName in SheetList)
                            {
                                if (SheetName == PoSheetName)
                                {
                                    //LogError("CommonMethods", "GetExcelData", new Exception("TEst5"));

                                    //Read Data from First Sheet
                                    dt = new DataTable();
                                    cmdExcel.CommandText = "SELECT * From [" + SheetName + "]";
                                    oda.SelectCommand = cmdExcel;
                                    oda.Fill(dt);

                                    LogError("CommonMethods", "GetExcelData", new Exception("TEst6"));

                                    dt.TableName = SheetName;
                                    ds.Tables.Add(DeleteEmptyRows(dt));

                                    //LogError("CommonMethods", "GetExcelData", new Exception("TEst7"));
                                }
                            }

                            //LogError("CommonMethods", "GetExcelData", new Exception("TEst8"));
                        }

                        //LogError("CommonMethods", "GetExcelData", new Exception("TEst9"));
                    }

                    //LogError("CommonMethods", "GetExcelData", new Exception("TEst10"));
                }
                //Return Dataset
                return ds;
            }
            catch (Exception err)
            {
                LogError("CommonMethods", "GetExcelData", err);
                return ds;
            }
            finally
            {
                oda.Dispose();
                if (connExcel.State == ConnectionState.Open)
                    connExcel.Close();
            }
        }

        private static DataTable DeleteEmptyRows(DataTable dt)
        {
            DataTable formattedTable = dt.Copy();
            List<DataRow> drList = new List<DataRow>();
            foreach (DataRow dr in formattedTable.Rows)
            {
                int count = dr.ItemArray.Length;
                int nullcounter = 0;
                for (int i = 0; i < dr.ItemArray.Length; i++)
                {
                    if (dr.ItemArray[i] == null || string.IsNullOrEmpty(Convert.ToString(dr.ItemArray[i])))
                    {
                        nullcounter++;
                    }
                }

                if (nullcounter == count)
                {
                    drList.Add(dr);
                }
            }

            for (int i = 0; i < drList.Count; i++)
            {
                formattedTable.Rows.Remove(drList[i]);
            }
            formattedTable.AcceptChanges();

            return formattedTable;
        }

        public static void LogTracking(string Pagename, string Methodname, string msg)
        {
            try
            {
                StringBuilder sb = new StringBuilder();

                sb.Append(string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
                sb.AppendLine("-----------------------------------------------------------");
                sb.AppendLine(string.Format("Pagename: {0}", Pagename));
                sb.AppendLine(string.Format("Methodname: {0}", Methodname));
                sb.AppendLine(string.Format("Message: {0}", msg));
                sb.AppendLine("-----------------------------------------------------------");

                string filePath = ConfigurationManager.AppSettings.Get("Tracklogfilepath");
                string path = HttpContext.Current.Server.MapPath(filePath);
                using (StreamWriter writer = new StreamWriter(path, true))
                {
                    writer.WriteLine(sb.ToString());
                    writer.Close();
                }
                sb.Clear();
            }
            catch
            {
            }
        }

    }
}
