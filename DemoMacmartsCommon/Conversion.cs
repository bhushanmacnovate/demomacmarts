﻿using Newtonsoft.Json;using System;using System.Collections.Generic;using System.ComponentModel;using System.Data;using System.IO;using System.Linq;using System.Reflection;using System.Security.Cryptography;using System.Text;using System.Threading.Tasks;namespace DemoMacmartsCommon{    public static class Conversion    {


        #region Extension And Conversion Methods
        public static bool DBNullToBoolean(this object value)        {            return Convert.ToBoolean((value == DBNull.Value ? false : value));        }        public static double DBNullToDouble(this object value)        {            return Convert.ToDouble((value == DBNull.Value ? 0 : value));        }        public static float DBNullToFloat(this object value)        {            return value == DBNull.Value ? 0 : float.Parse(value.DBNullToString());        }        public static string DBNullToString(this object value)        {            return value == DBNull.Value ? string.Empty : Convert.ToString(value);        }        public static Int16 DBNullToInt16(this object value)        {            return Convert.ToInt16((value == DBNull.Value ? 0 : value));        }        public static Int32 DBNullToInt32(this object value)        {            return Convert.ToInt32((value == DBNull.Value ? 0 : value));        }        public static Int64 DBNullToInt64(this object value)        {            return Convert.ToInt64((value == DBNull.Value ? 0 : value));        }        public static Decimal DBNullToDecimal(this object value)        {            return Convert.ToDecimal((value == DBNull.Value ? 0 : value));        }        public static Decimal? ToNullableDecimal(this object value)        {            return Convert.ToDecimal((value == DBNull.Value ? 0 : value));        }        public static string FormatNumber(this object value)        {            try            {                decimal num = value.DBNullToDecimal();                if (num >= 100000000)                    return (num / 1000000M).ToString("0.#M");                if (num >= 1000000)                    return (num / 1000000M).ToString("0.#M");                if (num >= 100000)                    return (num / 1000M).ToString("0.#k");                if (num >= 10000)                    return (num / 1000M).ToString("0.#k");                if (num >= 1000)                    return (num / 1000).ToString("0.#k");                return num.ToString();            }            catch (Exception ex)            {                throw ex;            }        }        public static string Encode(this string strToEncode)        {            byte[] b = System.Text.ASCIIEncoding.ASCII.GetBytes(strToEncode);            string encodePDWring = Convert.ToBase64String(b);            return encodePDWring;        }        public static string ReplaceSpecialChar(this string value)        {            return value.Replace("&", " &amp;").Replace("<", "&lt;").Replace(">", "&gt;").Replace("\"", "&quot;").Replace("'", "");        }        public static string ToJson(this object value)        {            return JsonConvert.SerializeObject(value);        }        public static string ToSerialize(this object value)        {            return JsonConvert.SerializeObject(value);        }        public static object ToDeserialize(this string value)        {            return JsonConvert.DeserializeObject(value);        }




        #endregion
        #region Decrypt
        public static string Decrypt(string strText)        {            string EncryptionKey = "SOCV2SPBNI99212GTYH";            strText = strText.Replace("!", "+");            byte[] cipherBytes = Convert.FromBase64String(strText);            using (Aes encryptor = Aes.Create())            {                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });                encryptor.Key = pdb.GetBytes(32);                encryptor.IV = pdb.GetBytes(16);                using (MemoryStream ms = new MemoryStream())                {                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))                    {                        cs.Write(cipherBytes, 0, cipherBytes.Length);                        cs.Close();                    }                    strText = Encoding.Unicode.GetString(ms.ToArray());                }            }            return strText;        }





        #endregion
        #region Encrypt
        public static string Encrypt(string strText)        {            string EncryptionKey = "SOCV2SPBNI99212GTYH";            byte[] clearBytes = Encoding.Unicode.GetBytes(strText);            using (Aes encryptor = Aes.Create())            {                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });                encryptor.Key = pdb.GetBytes(32);                encryptor.IV = pdb.GetBytes(16);                using (MemoryStream ms = new MemoryStream())                {                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))                    {                        cs.Write(clearBytes, 0, clearBytes.Length);                        cs.Close();                    }                    strText = Convert.ToBase64String(ms.ToArray());                }            }            strText = strText.Replace("+", "!");            return strText;        }








        #endregion
        #region Encode

        #endregion
        #region Convert Generic List to Datatable        public static DataTable ToDataTable<T>(List<T> items)        {            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);            foreach (PropertyInfo prop in Props)            {
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name);            }            foreach (T item in items)            {                var values = new object[Props.Length];                for (int i = 0; i < Props.Length; i++)                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);                }                dataTable.Rows.Add(values);            }
            //put a breakpoint here and check datatable
            return dataTable;        }        public static DataTable ConvertListToDataTable<T>(IList<T> data)        {            PropertyDescriptorCollection properties =            TypeDescriptor.GetProperties(typeof(T));            DataTable table = new DataTable();            foreach (PropertyDescriptor prop in properties)                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);            foreach (T item in data)            {                DataRow row = table.NewRow();                foreach (PropertyDescriptor prop in properties)                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;                table.Rows.Add(row);            }            return table;        }        public static List<T> DataTableToList<T>(this DataTable table) where T : class, new()        {            try            {                List<T> list = new List<T>();                foreach (var row in table.AsEnumerable())                {                    T obj = new T();                    foreach (var prop in obj.GetType().GetProperties())                    {                        try                        {                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);                        }                        catch                        {                            continue;                        }                    }                    list.Add(obj);                }                return list;            }            catch            {                return null;            }        }


        #endregion    }}