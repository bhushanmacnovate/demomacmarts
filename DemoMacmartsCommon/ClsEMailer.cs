﻿using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Runtime.Remoting.Messaging;
using System.Net.Mail;

namespace DemoMacmartsCommon
{
    public class ClsEMailer : IDisposable
    {
        #region variables

        string _SMTPServer;
        string _SMTPport;
        string _MlFrom, _MlFrom1;
        string _MlTo;
        string _MlCCed;
        string _MlBCCed;
        string _Sub;
        string _MlFormat;
        string _Host;
        //MailPriority _MlPriority;
        string _Body;
        string _SenderName;
        int _Port;
        string _EmailAttachments;
        bool _IsError;
        string _UserName;
        string _Password;
        MailPriority _MlPriority;
        Exception _GotException;
        string AllowEmail = ConfigurationManager.AppSettings["AllowEmail"].ToString();
        string ClientId = ConfigurationManager.AppSettings["ClientId"].ToString();
        string UseDefaultCredentials = ConfigurationManager.AppSettings["UseDefaultCredentials"].ToString();
        string EnableSsl = ConfigurationManager.AppSettings["EnableSsl"].ToString();
        string DeliveryMethod = ConfigurationManager.AppSettings["DeliveryMethod"].ToString();
        string _VerifyEmailURL;
        long _RequisitionId;
        System.Net.Mail.MailMessage _MlMsg;
        System.Net.Mail.SmtpClient _MlClient;

        #endregion

        #region Constructor
        public ClsEMailer()
        {
            _Host = ConfigurationManager.AppSettings["SmtpServer"].ToString();
            _Port = Convert.ToInt32(ConfigurationManager.AppSettings["SmtpPort"]);
            _UserName = ConfigurationManager.AppSettings["smtpUserName"].ToString();
            _Password = ConfigurationManager.AppSettings["smtpPassword"].ToString();
            _MlFrom = ConfigurationManager.AppSettings["ApplicationEmailId"].ToString();
            _MlTo = string.Empty;
            _MlCCed = string.Empty;
            _MlBCCed = string.Empty;
            _MlPriority = MailPriority.Normal;
            _EmailAttachments = string.Empty;
            _Sub = string.Empty;
            _Body = string.Empty;
           
            _IsError = false;
            _RequisitionId = 0;
            _VerifyEmailURL = ConfigurationManager.AppSettings["VerifyEmailLink"].ToString();

        }
        public ClsEMailer(string SMTPServer, string SMTPPort)
        {
            _SMTPServer = "";
            _SMTPport = "";
            _MlFrom = "";
            _MlTo = "";
            _MlCCed = string.Empty;
            _MlBCCed = string.Empty;
            //_MlPriority = MailPriority.Normal;
            _EmailAttachments = string.Empty;
            _Sub = string.Empty;
            _Body = string.Empty;
            _MlFormat = "html";
            _IsError = false;
            _Host = "";
            _RequisitionId = 0;
            SMTPServer.Trim();
            SMTPPort.Trim();

            _VerifyEmailURL = string.Empty;

            if (SMTPServer.Length == 0)
                return;
            else
                _SMTPServer = SMTPServer;

            if (SMTPPort.Length == 0)
                return;
            else
                _SMTPport = SMTPPort;

            return;
        }
        #endregion

        #region Disposable

        /// <summary>
        /// Enable server control to final clean up before it is released from memory
        /// </summary>
        public void Dispose()
        {

        }
        #endregion

        #region properties

        /// <summary>
        /// Gets or sets the name of the SMTP sever to be used
        /// </summary>
        public string MailServer
        {

            get { return _SMTPServer; }
            set
            {
                value.Trim();
                if (value.Length == 0)
                    throw new Exception("Mail Server cannot be blank.");
                else
                {
                    _SMTPServer = value.ToString();
                }
            }
        }

        /// <summary>
        /// Gets or sets the port number of the SMTP sever to be used
        /// </summary>
        public string MailPort
        {
            get { return _SMTPport; }
            set
            {
                value.Trim();
                if (value.Length == 0)
                    throw new Exception("Mail port no is blank.");
                else
                {
                    if (!IsNumeric(value.ToString()))
                        throw new Exception("Invalid Mail port no.");
                    else
                        _SMTPport = value.ToString();
                }
            }
        }

        /// <summary>
        /// Gets or sets valid FROM email address for sending the email
        /// </summary>
        public string MailFromID
        {
            get { return _MlFrom.ToString(); }
            set
            {
                value.Trim();
                if (value.Length > 0)
                {
                    _MlFrom = value.ToString();
                }
                else
                    throw new Exception("No from ID mentioned.");

            }
        }

        /// <summary>
        /// Gets and sets semi-colon (";") separted list of email recipients
        /// </summary>
        public string MailReciepientsTo
        {
            get { return _MlTo; }
            set
            {
                value.Trim();
                if (value.Length <= 0)
                    throw new Exception("At least one receipients required to send mails.");
                else
                {
                    _MlTo = value.ToString();
                }
            }
        }

        /// <summary>
        /// Gets and sets semi-colon (";") separted list of email carbon copies(Cc)
        /// </summary>
        public string MailReciepientsCC
        {
            get { return _MlCCed; }
            set { _MlCCed = value.ToString(); }
        }

        /// <summary>
        /// Gets or sets semi-colon (";") separted list of email blind carbon copies
        /// </summary>
        public string MailReciepientsBCC
        {
            get { return _MlBCCed; }
            set { _MlBCCed = value.ToString(); }
        }


        /// <summary>
        /// Gets or sets subject of the email
        /// </summary>
        public string Subject
        {
            get { return _Sub; }
            set { _Sub = value.ToString(); }
        }

        public long RequisitionId
        {
            get { return _RequisitionId; }
            set { _RequisitionId = Convert.ToInt64(value); }
        }
        /// <summary>
        /// Gets or sets format of the email
        /// </summary>
        public string Format
        {
            get { return _MlFormat; }
            set { _MlFormat = value.ToString(); }
        }

        /// <summary>
        /// Gets or sets priority of the email
        /// </summary>
        //public MailPriority Priority
        //{
        //    get { return _MlPriority; }
        //    set { _MlPriority = value; }
        //}

        /// <summary>
        /// Gets or sets message body for the email
        /// </summary>
        public string BodyMatter
        {
            get { return _Body; }
            set { _Body = value.ToString(); }
        }
        public string SenderName
        {
            get { return _SenderName; }
            set { _SenderName = value.ToString(); }
        }

        /// <summary>
        /// Gets or sets semi-colon (";") separated list email attachment paths
        /// </summary>
        public string EmailAttachments
        {
            get { return _EmailAttachments; }
            set { _EmailAttachments = value.ToString(); }
        }

        /// <summary>
        /// gets whether error occurs while sending mail
        /// </summary>
        public bool IsError
        {
            get { return _IsError; }
        }




        /// <summary>
        /// Gets exception detail of email
        /// </summary>
        public Exception ExceptionDetail
        {
            get { return _GotException; }
        }


        public string VerifyEmailURL
        {
            get { return _VerifyEmailURL; }

        }

        #endregion

        #region privatemethods


        private bool IsNumeric(string temp)
        {
            try
            {
                Int32.Parse(temp);
            }
            catch
            {
                return false;
            }
            return true;
        }

        private Exception RaiseError(string Msg)
        {
            _IsError = true;
            Exception _ObjExlocal = new Exception(Msg);
            return _ObjExlocal;
        }
        #endregion

        #region Asychronous Methods

        public delegate bool SendEmailDelegate();


        public void GetResultsOnCallback(IAsyncResult ar)
        {
            SendEmailDelegate del = (SendEmailDelegate)
             ((AsyncResult)ar).AsyncDelegate;
            try
            {
                bool result;
                result = (bool)del.EndInvoke(ar);
            }
            catch
            {
            }
        }

        //public string SendEmailAsync()
        //{
        //    SendEmailDelegate dc = new SendEmailDelegate(this.SendMail);
        //    AsyncCallback cb = new AsyncCallback(this.GetResultsOnCallback);
        //    IAsyncResult ar = dc.BeginInvoke(cb, null);
        //    return "ok";
        //}

        #endregion Asychronous Methods

        #region publicMethods

        public bool SendMail()
        {

            System.Net.Mail.MailMessage _MlMsg = new MailMessage();
            bool isSent = false;
            string Error = "";
            try
            {
                
                _MlMsg.From = new System.Net.Mail.MailAddress(_SenderName + "<"+_MlFrom.ToString()+">");
                
                string[] AllToID = _MlTo.Split(';');

                foreach (string e in AllToID)
                {
                    if (e != "")
                    {
                        _MlMsg.To.Add(new System.Net.Mail.MailAddress(e));
                    }
                }

                if (_MlCCed.Length > 0)
                {
                    string[] AllCcID = _MlCCed.Split(';');
                    foreach (string e in AllCcID)
                    {
                        if (e != "")
                        {
                            _MlMsg.CC.Add(new System.Net.Mail.MailAddress(e));
                        }
                    }

                }
                if(_EmailAttachments !="")
                {
                    System.Net.Mail.Attachment attachment = new Attachment(_EmailAttachments);
                    _MlMsg.Attachments.Add(attachment);
                }
                _MlMsg.Subject = _Sub.ToString();

                _MlMsg.Priority = _MlPriority;

                if (_MlBCCed.Length > 0)
                {
                    string[] AllBCcID = _MlBCCed.Split(';');
                    foreach (string e in AllBCcID)
                    {
                        if (e != "")
                        {
                            _MlMsg.Bcc.Add(new System.Net.Mail.MailAddress(e));
                        }
                    }

                }
                _MlMsg.IsBodyHtml = true;
                _MlMsg.Subject = _Sub.ToString();
                _MlMsg.Body = _Body.ToString();
             
                _MlClient = new SmtpClient();

                if (Convert.ToBoolean(UseDefaultCredentials) == true)
                {
                    _MlClient.UseDefaultCredentials = true;
                    if (_UserName != "" && _Password != "")
                        _MlClient.Credentials = new System.Net.NetworkCredential(_UserName, _Password);
                }
                else
                {
                    _MlClient.UseDefaultCredentials = false;
                }                
                
                _MlClient.Host = _Host;

                if (Convert.ToBoolean(DeliveryMethod) == true)
                {
                    
                    _MlClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                               

                if (Convert.ToBoolean(EnableSsl) == true)
                {
                    _MlClient.EnableSsl = true;
                }
                else
                {
                    _MlClient.EnableSsl = false;
                }
                    
                _MlClient.Port = _Port;
                if (_MlTo != null && _MlTo != "")
                {
                    if (AllowEmail == "yes")
                    {
                        _MlClient.Send(_MlMsg);
                    }
                    isSent = true;

                }
                else
                {
                    isSent = false;

                }

                Error = "";
            }
            catch (Exception ObjEx)
            {
                isSent = false;
                _IsError = true;
                _GotException = ObjEx;
                Error = ObjEx.ToString();
                //CommonMethods.LogError("ClsEMailer", "SendEmail", ObjEx);
                return false;
            }
            finally
            {
            }

            long id = 0;
            //UpdateInDatabase(_Sub.ToString(), _Body.ToString(), _MlTo, _MlCCed, _MlBCCed, isSent, Error, _RequisitionId, out id);

            return isSent;
        }

        #endregion
    }
}
