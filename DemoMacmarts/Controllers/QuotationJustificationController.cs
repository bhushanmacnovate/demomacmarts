﻿using DemoMacmartsBL;
using DemoMacmartsBL.BL;
using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMacmarts.Controllers
{
    public class QuotationJustificationController : Controller
    {
        // GET: SupplierJustification
      public ActionResult Index()
            {
            //UserRoleBL objRoleBL = new UserRoleBL();
            //if (objRoleBL.CheckPageAccess(PageId))
            //{
                SupplierJustificationMainVM obj = new SupplierJustificationMainVM();
                SupplierJustificationVM objPtype = new SupplierJustificationVM();
                SupplierJustificationBL objbl = new SupplierJustificationBL();
                obj.JustificationList = objbl.LoadAllSupplierJustifications();
                obj.SupplierJustification = objPtype;
                return View("SupplierJustification", obj);
            //}
            //else
            //{
            //    return View("Unauthorized");
            //}

        }
        public string AddSupplierJustification(string SupplierJustification)
        {
            string result = string.Empty;
            SupplierJustificationBL objbl = new SupplierJustificationBL();
            result = objbl.AddSupplierJustification(SupplierJustification);
            return result;
        }
        public string UpdateSupplierJustification(string SupplierJustification, int id)
        {
            string result = string.Empty;
            SupplierJustificationBL objbl = new SupplierJustificationBL();
            result = objbl.UpdateSupplierJustification(SupplierJustification, id);
            return result;
        }
        public ActionResult LoadAllSupplierJustifications()
        {
            SupplierJustificationListVM obj = new SupplierJustificationListVM();
            SupplierJustificationBL objbl = new SupplierJustificationBL();
            obj = objbl.LoadAllSupplierJustifications();
            return PartialView("_SupplierJustificationGrid", obj);
        }
        public ActionResult LoadSingleSupplierJustification(int id)
        {
            SupplierJustificationVM obj = new SupplierJustificationVM();
            SupplierJustificationBL objbl = new SupplierJustificationBL();
            obj = objbl.LoadSingleSupplierJustification(id);
            return PartialView("_SupplierJustificationAdd", obj);
        }
        public string DeleteSupplierJustification(int id)
        {
            string result = string.Empty;
            SupplierJustificationBL objbl = new SupplierJustificationBL();
            result = objbl.DeleteSupplierJustification(id);
            return result;
        }
    }
}