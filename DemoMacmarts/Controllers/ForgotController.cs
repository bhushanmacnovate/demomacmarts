﻿using DemoMacmartsBL.BL.Forgot;
using DemoMacmartsCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MacMacmartsWeb.Controllers
{
    public class ForgotController : Controller
    {
        // GET: Forgot
        public ActionResult Index()
        {
            if (Request.QueryString["code"] != null)
            {
               ForgotPassBL  objbl = new ForgotPassBL();
                ViewBag.Message = objbl.CheckResetPassword(Convert.ToString(Request.QueryString["code"]));
                return View("changepassword");
            }
            else
            {
                return View("forgot");
            }
        }

        public string SendPassword(string email)
        {
            string result = "";
            ForgotPassBL bl = new ForgotPassBL();
            result = bl.CheckPassword(email);
            return result;
        }
        [HttpPost]
        public string ResetPassword()
        {
            string result = "";

            string code = Request.Form["txtcode"].ToString();
            string newp = Request.Form["txtnewp"].ToString();
            if (code != "")
            {
                ForgotPassBL objbl = new ForgotPassBL();
                result = objbl.CheckResetPassword(code, newp);
            }
            else
            {
                result = "Invalid Link!";
            }



            return result;
        }

    }
}