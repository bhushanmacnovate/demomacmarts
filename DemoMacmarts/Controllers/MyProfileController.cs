﻿using DemoMacmartsBL;
using DemoMacmartsBL.BL.MyProfile;
using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace MacMacmartsWeb.Controllers
{
    public class MyProfileController : Controller
    {
        // GET: MyProfile
        public ActionResult Index()
        {
            UserVM obj = new UserVM();
            long UserId = UserVM._UserID;
            MyProfileBL objbl = new MyProfileBL();
            obj = objbl.GetUserDetails(UserId);
            return View("MyProfile", obj);
        }

        public JsonResult UpdateProfile(string model)
        {
            MyProfileBL objbl = new MyProfileBL();
            UserVM obj = Newtonsoft.Json.JsonConvert.DeserializeObject<UserVM>(model);
            UserVM _objusers = new UserVM();
            _objusers.FirstName = obj.FirstName;
            _objusers.LastName = obj.LastName;
            _objusers.Phone = obj.Phone;
            _objusers.AltPhoneNo = obj.AltPhoneNo;
            _objusers.AltEmailId = obj.AltEmailId;
            //_objusers.Phone2 = obj.Phone2;
            //if(obj.UserPic!="" && obj.UserPic!=null )
            //{
            //    _objusers.UserPic = obj.UserPic;
            //}
            //else
            //{
            //    _objusers.UserPic = "default.png";
            //}

            long UserId = UserVM._UserID;
            _objusers.UserId = UserVM._UserID;

            string result = objbl.UpdateUserProfile(_objusers);

            if (result == "success")
            {
                result = "OK";
                UserVM._UserName = _objusers.FirstName + " " + _objusers.LastName;
                // UsersVM._UserPic = _objusers.UserPic;
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public string ChangePassword()
        {
            string result = "";
            long UserId = UserVM._UserID;
            string oldpassword = Request.Form["txtoldp"].DBNullToString();
            EncryptDecrypt objEncrypt = new EncryptDecrypt();
            string oldp = objEncrypt.Encrypt(oldpassword);
            string newpassword = Request.Form["txtnewp"].DBNullToString();
            string newp = objEncrypt.Encrypt(newpassword);
            MyProfileBL objbl = new MyProfileBL();
            result = objbl.ChangePassword(oldp, newp, UserId);
            return result;
        }

    }
}