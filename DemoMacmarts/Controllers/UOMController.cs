﻿using DemoMacmartsBL;
using DemoMacmartsBL.BL;
using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMacmarts.Controllers
{
    public class UOMController : Controller
    {
        // GET: UOM
        public ActionResult Index()
        {          
                UOMMainVM obj = new UOMMainVM();
                UnitOfMeasureVM objPtype = new UnitOfMeasureVM();
                UOMBL objbl = new UOMBL();
                obj.UOMList = objbl.LoadAllUOMs();
                obj.UOM = objPtype;
                return View("UOM", obj);
            }
        public string AddUOM(string UOM, string Description)
        {
            string result = string.Empty;
            UOMBL objbl = new UOMBL();
            result = objbl.AddUOM(UOM, Description);
            return result;
        }
        public string UpdateUOM(string UOM, string Description, int id)
        {
            string result = string.Empty;
            UOMBL objbl = new UOMBL();
            result = objbl.UpdateUOM(UOM, Description, id);
            return result;
        }
        public ActionResult LoadAllUOMs()
        {
            UOMListVM obj = new UOMListVM();
            UOMBL objbl = new UOMBL();
            obj = objbl.LoadAllUOMs();
            return PartialView("_UOMGrid", obj);
        }
        public ActionResult LoadSingleUOM(int id)
        {
            UnitOfMeasureVM obj = new UnitOfMeasureVM();
            UOMBL objbl = new UOMBL();
            obj = objbl.LoadSingleUOM(id);
            return PartialView("_UOMAdd", obj);
        }
        public string DeleteUOM(int id)
        {
            string result = string.Empty;
            UOMBL objbl = new UOMBL();
            result = objbl.DeleteUOM(id);
            return result;
        }
    }
 }