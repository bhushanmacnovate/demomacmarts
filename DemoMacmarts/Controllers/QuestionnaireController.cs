﻿using DemoMacmartsBL;
using DemoMacmartsBL.BL;
using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DemoMacmarts.Controllers
{
    public class QuestionnaireController : Controller
    {
        // GET: Questionnaire
        public ActionResult Index()
        {
            QuestionnaireMainVM obj = new QuestionnaireMainVM();
            QuestionnaireVM objPtype = new QuestionnaireVM();
            QuestionnaireBL objbl = new QuestionnaireBL();
            obj.QuestionnaireList = objbl.LoadAllQuestionnairs();
            obj.Questionnaire = objPtype;
            return View("Questionnaire", obj);
        }

        public string AddQuestionnaire(string itemName, string description, string QuestionType, bool Inputdata)
        {
            string result = string.Empty;
            QuestionnaireBL objbl = new QuestionnaireBL();
            result = objbl.AddQuestionnair(itemName, description, QuestionType , Inputdata);
            return result;
        }
        public string UpdateQuestionnaire(string itemName, string description, string QuestionType, int id, bool Inputdata)
        {
            string result = string.Empty;
            QuestionnaireBL objbl = new QuestionnaireBL();
            result = objbl.UpdateQuestionnair(itemName, description, QuestionType, id, Inputdata);
            return result;
        }
        public ActionResult LoadAllQuestionnaires()
        {
            QuestionnaireListVM obj = new QuestionnaireListVM();
            QuestionnaireBL objbl = new QuestionnaireBL();
            obj = objbl.LoadAllQuestionnairs();
            return PartialView("_QuestionnaireGrid", obj);
        }
        public ActionResult LoadSingleQuestionnaire(int id)
        {
            QuestionnaireVM obj = new QuestionnaireVM();
            QuestionnaireBL objbl = new QuestionnaireBL();
            obj = objbl.LoadSingleQuestionnaire(id);
            return PartialView("_QuestionnaireAdd", obj);
        }
        public string DeleteQuestionnaire(int id)
        {
            string result = string.Empty;
            QuestionnaireBL objbl = new QuestionnaireBL();
            result = objbl.DeleteQuestionnaire(id);
            return result;
        }
    }
}