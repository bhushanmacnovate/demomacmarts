﻿using DemoMacmartsBL;
using DemoMacmartsBL.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DemoMacmartsCommon;

namespace DemoMacmarts.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            
            
            return View("Login");
        }
        public ActionResult Registration()
        {


            return View("~/Views/MyProfile/Index");
        }
        [HttpPost]
        public string CheckLogin(string email, string password)
        {
            string result = "";
            LoginBL bl = new LoginBL();
            EncryptDecrypt objEncrypt = new EncryptDecrypt();
            password = objEncrypt.Encrypt(password);
            UserVM _objuser = bl.CheckLogin(email, password);
            if(_objuser.status == "Valid")
            {
                UserVM._UserName = _objuser.FirstName + ' ' + _objuser.LastName;
                UserVM._UserID = _objuser.UserId;
                result = "Valid";
            }
            else
            {
                result = "Invalid";
            }
            return result;
        }
    }
}