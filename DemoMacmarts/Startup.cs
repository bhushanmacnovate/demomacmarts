﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DemoMacmarts.Startup))]
namespace DemoMacmarts
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
