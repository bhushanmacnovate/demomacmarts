/*
 *  Document   : uiTables.js
 *  Author     : pixelcave
 *  Description: Custom javascript code used in Tables page
 */

var UiTables = function () {

    return {
        init: function () {
            /* Initialize Bootstrap Datatables Integration */
            App.datatables();


            /* Initialize Datatables */
            $('.macmart-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,                
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });



           
        },

        project: function () {
        App.datatables();
            //----Proj grid
            $('#projecttype-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });

        },

        dept: function () {
            App.datatables();
            //---------dept grid

            $('#gridDepartments').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        Bank: function () {
            App.datatables();
            //---------dept grid

            $('#bank-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        Accounttype: function () {
            App.datatables();
            //---------dept grid

            $('#accounttype-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        Expertise: function () {
            App.datatables();
            //---------dept grid

            $('#Expertise-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        DesignItem: function () {
            App.datatables();
            //---------dept grid

            $('#designitem-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        GLCodeGrid: function () {
            App.datatables();
            //---------dept grid

            $('#glcodegrid-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        CostCenterGrid: function () {
            App.datatables();
            //---------dept grid

            $('#costcentergrid-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        QuestionGrid: function () {
            App.datatables();
            //---------dept grid

            $('#questiongrid-datatable').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        PendingQuote: function () {
            App.datatables();
            //---------dept grid

            $('#pending-qoutes').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[1, "desc"]],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        ApprovedQuote: function () {
            App.datatables();
            //---------dept grid

            $('#tblapprovedproject').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[1, "desc"]],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        EmailLog: function () {
            App.datatables();
            //---------dept grid

            $('#EmailLog').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[ 0, "desc" ]],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        ProjectGrid: function () {
            App.datatables();
            //---------dept grid

            $('#tblprojectgrid').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[2, "desc"]],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        ProjectGrid1: function () {
            App.datatables();
            //---------dept grid

            $('#tblprojectgrid').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[0, "desc"]],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        InventoryGrid: function () {
            App.datatables();
            //---------dept grid

            $('#tblprojectgrid').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[0, "desc"]],
                "iDisplayLength": 50,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
        NotificationGrid: function () {
            App.datatables();
            //---------dept grid

            $('#tblnotificationgrid').dataTable({
                "aoColumnDefs": [{ "bSortable": false }],
                "order": [[2, "desc"]],
                "iDisplayLength": 20,
                "aLengthMenu": [[5, 10, 15, 20, 50], [5, 10, 15, 20, 50]],
                "FixedHeader ": top,
                "fnDrawCallback": function () {
                    var wrapper = this.parent();
                    var rowsPerPage = this.fnSettings()._iDisplayLength;
                    var rowsToShow = this.fnSettings().fnRecordsDisplay();
                    var minRowsPerPage = this.fnSettings().aLengthMenu[0][0];
                    if (rowsToShow <= rowsPerPage || rowsPerPage == -1) {
                        $('.dataTables_paginate', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_paginate', wrapper).css('visibility', 'visible');
                    }
                    if (rowsToShow <= minRowsPerPage) {
                        $('.dataTables_length', wrapper).css('visibility', 'hidden');
                    }
                    else {
                        $('.dataTables_length', wrapper).css('visibility', 'visible');
                    }
                }
            });

            /* Add placeholder attribute to the search input */
            $('.dataTables_filter input').attr('placeholder', 'Search');

            /* Select/Deselect all checkboxes in tables */
            $('thead input:checkbox').click(function () {
                var checkedStatus = $(this).prop('checked');
                var table = $(this).closest('table');

                $('tbody input:checkbox', table).each(function () {
                    $(this).prop('checked', checkedStatus);
                });
            });

            /* Table Styles Switcher */
            var genTable = $('#general-table');
            var styleBorders = $('#style-borders');

            $('#style-default').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').removeClass('table-borderless');
            });

            $('#style-bordered').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-borderless').addClass('table-bordered');
            });

            $('#style-borderless').on('click', function () {
                styleBorders.find('.btn').removeClass('active');
                $(this).addClass('active');

                genTable.removeClass('table-bordered').addClass('table-borderless');
            });

            $('#style-striped').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-striped');
                } else {
                    genTable.removeClass('table-striped');
                }
            });

            $('#style-condensed').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-condensed');
                } else {
                    genTable.removeClass('table-condensed');
                }
            });

            $('#style-hover').on('click', function () {
                $(this).toggleClass('active');

                if ($(this).hasClass('active')) {
                    genTable.addClass('table-hover');
                } else {
                    genTable.removeClass('table-hover');
                }
            });
        },
    };
}();