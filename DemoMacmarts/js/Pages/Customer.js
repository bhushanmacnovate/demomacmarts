﻿function validate() {
    //alert('');
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#CustName').val() == '') {
        ShowMessage('Please enter Customer Name', "no", "Customers");
        $('#CustName').focus();
        return false;
    }
    else if ($('#Address1').val() == '') {
        ShowMessage('Please enter Address Line 1.', "no", "Customers");
        $('#Address1').focus();
        return false;
    }   
    else if ($('#CountryId').val() == '') {
        ShowMessage('Please select Country.', "no", "Customers");
        $('#CountryId').focus();
        return false;
    }
    //else if ($('#StateName').val() == '') {
    //    ShowMessage('Please enter State Name.', "no", "Customers");
    //    $('#StateName').focus();
    //    return false;
    //}
    else if ($('#CityName').val() == '') {
        ShowMessage('Please enter City Name.', "no", "Customers");
        $('#CityName').focus();
        return false;
    }
    else if ($('#Phone1').val() == '') {
        ShowMessage('Please enter Phone.', "no", "Customers");
        $('#Phone1').focus();
        return false;
    }
    else if ($('#ContactPerson').val() == '') {
        ShowMessage('Please enter Contact Person.', "no", "Customers");
        $('#ContactPerson').focus();
        return false;
    }
    else if (!emailReg.test($('#ContactEmail').val())) {
        ShowMessage('Please enter valid Email Id', "no", "Customers");
        $('#ContactEmail').focus();
        return false;
    }
    //else if ($('#AuthType').val() == '0') {
    //    ShowMessage('Please select Authentication Type.', "no", "Customers");
    //    return false;
    //}
    else {

        var _ptModel = {};
        _ptModel.ClientId = $("#ClientId").val() == null ? "0" : $("#ClientId").val();

        _ptModel.CustName = $("#CustName").val();     
        _ptModel.Address1 = $("#Address1").val();
        _ptModel.Address2 = $("#Address2").val();
        _ptModel.ContactPerson = $("#ContactPerson").val();
        _ptModel.ContactEmail = $("#ContactEmail").val();
        _ptModel.CountryId = $("#CountryId").val();
        _ptModel.StateName = $("#StateName").val();
        _ptModel.CityName = $("#CityName").val();
        _ptModel.PinCode = $("#PinCode").val();  
        _ptModel.Phone1 = $("#Phone1").val();
        //_ptModel.Phone2 = $("#Phone2").val();
        _ptModel.VatNo = $("#VatNo").val();
        _ptModel.RegNo = $("#RegNo").val();
        _ptModel.URL = $("#URL").val();
        _ptModel.AuthTypeId = $("#AuthType").val();
        _ptModel.BankName = $("#BankName").val();
        _ptModel.BankBranch = $("#BankBranch").val();
        _ptModel.SwiftCode = $("#SwiftCode").val();
        _ptModel.AccountNumber = $("#AccountNumber").val();
        _ptModel.IBN = $("#IBN").val();
        _ptModel.isActive = document.getElementById('chkisactive').checked;

        var model = JSON.stringify(_ptModel);

        $.post('/Customers/Customers/AddNewCustomer', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Customer Added Successfully', "yes", "Customers", "/Customers/Customers");
            }
            else { ShowMessage(data.Result, "no", "Customers"); }

        });
    }
}
function Updatevalidate() {
    //alert('');
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#ClientId').val() == '0') {
        ShowMessage('Please Select Customer', "no", "Customers", "/Customers/Customers");
        return false;
    }
    if ($('#CustName').val() == '') {
        ShowMessage('Please enter Customer Name', "no", "Customers");
        $('#CustName').focus();
        return false;
    }
    else if ($('#Address1').val() == '') {
        ShowMessage('Please enter Address Line 1.', "no", "Customers");
        $('#Address1').focus();
        return false;
    }
    else if ($('#CountryId').val() == '') {
        ShowMessage('Please select Country.', "no", "Customers");
        $('#CountryId').focus();
        return false;
    }
    //else if ($('#StateName').val() == '') {
    //    ShowMessage('Please enter State Name.', "no", "Customers");
    //    $('#StateName').focus();
    //    return false;
    //}
    else if ($('#CityName').val() == '') {
        ShowMessage('Please enter City Name.', "no", "Customers");
        $('#CityName').focus();
        return false;
    }
    else if ($('#Phone1').val() == '') {
        ShowMessage('Please enter Phone.', "no", "Customers");
        $('#Phone1').focus();
        return false;
    }
    else if ($('#ContactPerson').val() == '') {
        ShowMessage('Please enter Contact Person.', "no", "Customers");
        $('#ContactPerson').focus();
        return false;
    }
    else if (!emailReg.test($('#ContactEmail').val())) {
        ShowMessage('Please enter valid Email Id', "no", "Customers");
        $('#ContactEmail').focus();
        return false;
    }
    //else if ($('#AuthType').val() == '0') {
    //    ShowMessage('Please select Authentication Type.', "no", "Customers");
    //    return false;
    //}
    else {

        var _ptModel = {};
        _ptModel.ClientId = $("#ClientId").val() == null ? "0" : $("#ClientId").val();

        _ptModel.CustName = $("#CustName").val();
        _ptModel.Address1 = $("#Address1").val();
        _ptModel.Address2 = $("#Address2").val();
        _ptModel.ContactPerson = $("#ContactPerson").val();
        _ptModel.ContactEmail = $("#ContactEmail").val();
        _ptModel.CountryId = $("#CountryId").val();
        _ptModel.StateName = $("#StateName").val();
        _ptModel.CityName = $("#CityName").val();
        _ptModel.PinCode = $("#PinCode").val();
        _ptModel.Phone1 = $("#Phone1").val();
        //_ptModel.Phone2 = $("#Phone2").val();
        _ptModel.VatNo = $("#VatNo").val();
        _ptModel.RegNo = $("#RegNo").val();
        _ptModel.URL = $("#URL").val();
        _ptModel.AuthTypeId = $("#AuthType").val();
        _ptModel.BankName = $("#BankName").val();
        _ptModel.BankBranch = $("#BankBranch").val();
        _ptModel.SwiftCode = $("#SwiftCode").val();
        _ptModel.AccountNumber = $("#AccountNumber").val();
        _ptModel.IBN = $("#IBN").val();
        _ptModel.isActive = document.getElementById('chkisactive').checked;
        var model = JSON.stringify(_ptModel);

        $.post('/Customers/Customers/UpdateCustomer', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Customer saved Successfully', "yes", "Customers", "/Customers/Customers");
            }
            else { ShowMessage(data.Result, "no", "Customers"); }

        });
    }
}
function cancel()
{
    window.location.href = '/Customers/Customers';
}