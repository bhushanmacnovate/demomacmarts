﻿$(function () {
    $('#btnsubmit').click(function () {
        debugger;
        //var usertype = document.getElementById('txtusertype').value;
        var hiddesignItemListId = document.getElementById('hiddesignItemListId');
        if ($('#ProjectType').val() == 'Design') {
            if (validateInsertProject()) {
                var itemIds = hiddesignItemListId.value.split(',');
                var ProjectItems = [];
                var qty = 0;
                for (var i = 0; i < itemIds.length; i++) {
                    debugger;
                    item = {};
                    item["ItemNo"] = document.getElementById('sp_itemno_' + itemIds[i]).innerHTML;
                    item["Description"] = document.getElementById('sp_itemdesc_' + itemIds[i]).innerHTML;
                    item["Quantity"] = document.getElementById('txtq_' + itemIds[i]).value;
                    item["UnitOfMeasure"] = document.getElementById('ddleditunitofmeasure_' + itemIds[i]).value;
                    item["Currency"] = document.getElementById('ddleditcurrency_' + itemIds[i]).value;
                    item["RequisitionId"] = 0;
                    if (document.getElementById('txtneedbydate_' + itemIds[i]).value != "") {
                        item["NeedByDate"] = document.getElementById('txtneedbydate_' + itemIds[i]).value.replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
                    }
                    else {
                        item["NeedByDate"] = "";
                    }


                    qty = qty + parseInt(document.getElementById('txtq_' + itemIds[i]).value);
                    ProjectItems.push(item);
                }
                if (qty > 0) {
                    jQuery.ajax({
                        url: '/RFQ/RFQ/SubmitDesignProjectItems',
                        type: "POST",
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify({ _obj: ProjectItems }),
                        success: function (data) {
                            debugger;
                            if (data == "success") {
                                //$('#txturs').val(CKEDITOR.instances.txturs.getData());
                                InsertProject();
                            }
                            else {
                                ShowMessage("Some error occurred", "no", "RFQ");
                                return false;
                            }

                        },
                        error: function (xhr) {
                            debugger;

                        }
                    });
                }
                else {
                    ShowMessage("Please check status of design item", "no", "RFQ");
                }
            }
        }
        else {
            if (validateInsertProject()) {
                InsertProject();
            }
        }
    });
});
function validateInsertProject() {

    debugger;

    var Today = new Date();
    var dd = Today.getDate();
    var mm = Today.getMonth() + 1; //January is 0!
    var yyyy = Today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var Today = dd + '/' + mm + '/' + yyyy;

    currentdate = Today.split('/');
    if (parseInt(currentdate[1]) == 1) {
        currentdate[1] = 12;
        currentdate[2] = parseInt(currentdate[2]) - parseInt(1);
    }
    else if (parseInt(currentdate[1]) == 12) {
        currentdate[1] = 1;
        currentdate[2] = parseInt(currentdate[2]) + parseInt(1);
    }
    else {
        currentdate[1] = parseInt(currentdate[1]) - parseInt(1)
    }

    var currentDateValue = new Date(currentdate[2], currentdate[1], currentdate[0]);

    var RequiredDate = $.trim($("[id$='RequiredDate']").val());
    var RequiredDatevalue = currentDateValue;
    if (RequiredDate != '') {
        dateRequiredDate = RequiredDate.split('/');
        if (parseInt(dateRequiredDate[1]) == 1) {
            dateRequiredDate[1] = 12;
            dateRequiredDate[2] = parseInt(dateRequiredDate[2]) - parseInt(1);
        }
        else if (parseInt(dateRequiredDate[1]) == 12) {
            dateRequiredDate[1] = 1;
            dateRequiredDate[2] = parseInt(dateRequiredDate[2]) + parseInt(1);
        }
        else {
            dateRequiredDate[1] = parseInt(dateRequiredDate[1]) - parseInt(1)
        }
        RequiredDatevalue = new Date(dateRequiredDate[2], dateRequiredDate[1], dateRequiredDate[0]);
    }

    var QuoteCutOffDate = $.trim(document.getElementById('QuoteCutOffDate').value);
    var QuoteCutOffDatevalue = currentDateValue;
    if (QuoteCutOffDate != '') {
        dateQuoteCutOffDate = QuoteCutOffDate.split('/');

        if (parseInt(dateQuoteCutOffDate[1]) == 1) {
            dateQuoteCutOffDate[1] = 12;
        }
        else if (parseInt(dateQuoteCutOffDate[1]) == 12) {
            dateQuoteCutOffDate[1] = 1;
        }
        else {
            dateQuoteCutOffDate[1] = parseInt(dateQuoteCutOffDate[1]) - parseInt(1)
        }

        QuoteCutOffDatevalue = new Date(dateQuoteCutOffDate[2], dateQuoteCutOffDate[1], dateQuoteCutOffDate[0]);
    }


    if ($('#ddlClientList').val() == '0') {
        ShowMessage('Please select Customer', "no", "RFQ");
        $('#ddlClientList').focus();
        return false;
    }
    else if ($('#ProjectType').val() == '') {
        ShowMessage('Please select Project Type', "no", "RFQ");
        $('#ProjectType').focus();
        return false;
    }
    else if ($('#ProjectCreationDate').val() == '') {
        ShowMessage('Please enter created date', "no", "RFQ");
        $('#ProjectCreationDate').focus();
        return false;
    }   
    else if ($('#ProjectDescription').val() == '') {
        ShowMessage('Please enter Description', "no", "RFQ");
        $('#ProjectDescription').focus();
        return false;
    }   
    else if ($('#RequiredDate').val() == '') {
        ShowMessage('Please enter Required Date', "no", "RFQ");
        //$('#RequiredDate').focus();
        return false;
    }
    else if (RequiredDatevalue <= currentDateValue) {
        ShowMessage('Required Date should greater than Todays Date.', "no", "RFQ");
        // $('#RequiredDate').focus();
    }
    else if ($('#QuoteCutOffDate').val() == '') {//userType == "M" && 
        ShowMessage('Please enter Quote Cutoff Date', "no", "RFQ");
        //$('#QuoteCutOffDate').focus();
        return false;
    }
    else if (QuoteCutOffDatevalue >= RequiredDatevalue) {
        ShowMessage('Quote Cut Off Date should be less than Required Date.', "no", "RFQ");
        // $('#RequiredDate').focus();
    }
    else if (QuoteCutOffDatevalue <= currentDateValue) {
        ShowMessage('Quote Cut Off Date should be greater than Todays Date.', "no", "RFQ");
        // $('#RequiredDate').focus();
    }
    else if (CheckToolingItems() == false) {
        ShowMessage('Please add items in Item List', "no", "RFQ");
        return false;
    }
    else if ($('#ProjectType').val() != 'Tooling' && $('#hidtotalitems').val() == '0') {
        ShowMessage('Please enter Items in Item List', "no", "RFQ");
        return false;
    }
    else if ($('#ProjectType').val() != 'Tooling' && $('#hidtotalsuppliers').val() == '0') {
        ShowMessage('Please select Suppliers', "no", "RFQ");
        return false;
    }    
    else {
        return true;
    }
}
function CheckToolingItems() {
    var result = true;
    if ($('#ProjectType').val() != 'Design') {
        if ($('#hidtotalitems').val() == '0') {
            result = false;
        }
    }
    return result;
}
function InsertProject() {

    var _IPModel = {};
    _IPModel.ClientId = $("#ddlClientList").val();
    _IPModel.ProjectType = $("#ProjectType").val();
    _IPModel.ProjectDescription = $("#ProjectDescription").val();
    if ($("#RequiredDate").val() != "") {
        var RequiredDate = $("#RequiredDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        _IPModel.RequiredDate = RequiredDate;
    }
    if ($("#QuoteCutOffDate").val() != "") {
        var QuoteCutOffDate = $("#QuoteCutOffDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        _IPModel.QuoteCutOffDate = QuoteCutOffDate;
    }

    _IPModel.Contact = $("#Contact").val();
    _IPModel.Reference = $("#Reference").val();
    _IPModel.ReqReference = $("#ReqReference").val();

    var pTmodel = JSON.stringify(_IPModel);

    $.post('/RFQ/RFQ/CreateProject', { pTmodel: pTmodel }, function (data) {

        if (data.Result == "OK") {
            debugger;
            ShowMessage('Project added successfully with Requisition Id : ' + data.RequisitionId, "yes", "RFQ", data.ReturnPageUrl);
        }
        else { ShowMessage(data.Result, "no", "RFQ"); }

    });
}