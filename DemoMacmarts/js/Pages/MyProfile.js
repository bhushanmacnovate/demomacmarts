﻿function UpdateProfile() {
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#FirstName').val() == '') {
        ShowMessage('Please enter First Name.', "no", "My Profile");
        $('#FirstName').focus();
        return false;
    }
    else if ($('#LastName').val() == '') {
        ShowMessage('Please enter Last Name.', "no", "My Profile");
        $('#LastName').focus();
        return false;
    }
    else if ($('#EmailId').val().trim() == '') {
        ShowMessage('Please enter Email Id', "no", "My Profile");
        $('#EmailId').focus();
        return false;
    }
    else if (!emailReg.test($('#EmailId').val())) {
        ShowMessage('Please enter valid EmailId', "no", "My Profile");
        $('#EmailId').focus();
        return false;
    }
    else if (!emailReg.test($('#AltEmailId').val())) {
        ShowMessage('Please enter valid Alternate Email Id', "no", "My Profile");
        $('#EmailId').focus();
        return false;
    }
  

    else {

        var _ptModel = {};
        _ptModel.UserId = $("#userid").val() == null ? "0" : $("#userid").val();
        _ptModel.FirstName = $("#FirstName").val();
        _ptModel.LastName = $("#LastName").val();
        _ptModel.AltEmailId = $("#AltEmailId").val();
        _ptModel.Phone = $("#Phone").val();
        _ptModel.AltPhoneNo = $("#AltPhoneNo").val();
        _ptModel.UserPic = $("#UserPic").val();

        var model = JSON.stringify(_ptModel);

        $.post('/MyProfile/UpdateProfile', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Profile saved successfully', "yes", "My Profile");
            }
            else { ShowMessage(data.Result, "no", "My Profile"); }

        });
    }
}

function ChangePassword() {
    if ($('#txtoldp').val() == '') {
        ShowMessage('Please enter old password', "no", "My Profile");
        return false;
    }
    else if ($('#txtnewp').val() == '') {
        ShowMessage('Please enter new password', "no", "My Profile");
        return false;
    }
    else if ($('#txtnewp').val().length < 5) {
        ShowMessage('Password length should be greater than 5 characters', "no", "My Profile");
        return false;
    }
    else if ($('#txtconfirmp').val() == '') {
        ShowMessage('Please enter confirm password', "no", "My Profile");
        return false;
    }
    else if ($('#txtconfirmp').val() != $('#txtnewp').val()) {
        ShowMessage('New password & confirm password does not match', "no", "My Profile");
        return false;
    }
    else {
        debugger;

        $.post("/MyProfile/ChangePassword", $("#frmchangepassword").serialize(), function (data) {
            debugger;
            if (data == 'success') {
                ShowMessage('Password Changed successfully.Please login with new Password', "yes", "My Profile", "/Login/Index");
                //window.location.href = '/Login/Index';
                return false;
            }
            else if (data == 'error') {
                ShowMessage('Error Updating Password.', "no", "My Profile")
                return false;
            }
            else {
                ShowMessage(data, "no", "My Profile");
                return false;
            }
        });
    }
}

function passwordStrength(password) {
    debugger;
    var desc = [{ 'width': '0px' }, { 'width': '20%' }, { 'width': '40%' }, { 'width': '60%' }, { 'width': '80%' }, { 'width': '100%' }];

    var descClass = ['', 'progress-bar-danger', 'progress-bar-danger', 'progress-bar-warning', 'progress-bar-success', 'progress-bar-success'];

    var score = 0;

    //if password bigger than 6 give 1 point
    if (password.length > 6) score++;

    //if password has both lower and uppercase characters give 1 point	
    if ((password.match(/[a-z]/)) && (password.match(/[A-Z]/))) score++;

    //if password has at least one number give 1 point
    if (password.match(/d+/)) score++;

    //if password has at least one special caracther give 1 point
    if (password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/)) score++;

    //if password bigger than 12 give another 1 point
    if (password.length > 10) score++;

    var strength = ' Password Strength : Low';
    if (score < 2) {
        strength = 'Password Strength : Low';
    }
    else if (score < 4) {
        strength = 'Password Strength : Medium';
    }
    else {
        strength = 'Password Strength : High';
    }
    // display indicator
    $("#lblpasswordindicator").html(strength);
}