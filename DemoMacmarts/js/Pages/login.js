﻿
$(document).ready(function () {
    //alert(getCookie('remember'));
    if (getCookie('remember') == 'yes') {
        $('#txtemail').val(getCookie('u'));
        $('#txtpassword').val(getCookie('p'));
        document.getElementById('login-remember-me').checked = true;
    }
    else
    {
        $('#txtemail').val('');
        $('#txtpassword').val('');
        document.getElementById('login-remember-me').checked = false;
    }

    $('#form-login').validate({
        errorClass: 'help-block animation-slideUp', // You can change the animation class for a different entrance animation - check animations page
        errorElement: 'div',
        errorPlacement: function (error, e) {
            e.parents('.form-group > div').append(error);
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
            $(e).closest('.help-block').remove();
        },
        success: function (e) {
            e.closest('.form-group').removeClass('has-success has-error');
            e.closest('.help-block').remove();
        },
        rules: {
            'txtemail': {
                required: true,
                email: true
            },
            'txtpassword': {
                required: true
            }
        },
        messages: {
            'txtemail': 'Please Enter Email Id',
            'txtpassword': {
                required: 'Please Enter Password'
            }
        }
    });
});

$('#txtemail,#txtpassword').keydown(function (e) {
    var key = e.which;
    console.log(key);
    if (key == 13)  // the enter key code
    {
        $('#btnlogin').trigger('click');
    }
});

$(document).keypress(function (e) {
    if (e.which == 13) {
        var email = $.trim($('#txtemail').val());
        var pwd = $.trim($('#txtpassword').val());
        if (email != '' && pwd != '')
            $('#btnlogin').trigger('click');


    }
});


function CreateCookie(name, value, days) {
    var expires;
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}

$(function () {
    $('#btnlogin').click(function () {
        if ($("#form-login").valid()) {
            CallLogin();
        }

    });
    $('#btnlogin365').click(function () {
        if ($("#form-login").valid()) {
            CallOffice365Login();
        }

    });
});


function CallLogin() {
    $("#hidein5sec").css('display', 'block');
    $("#hidein5sec").fadeIn();

    var localTime = new Date();
    var Offset = createOffset(localTime);
    var TimeZone = localTime;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Login/CheckLogin",
        data: "{email : '" + $('#txtemail').val() + "', password : '" + $('#txtpassword').val() + "', TimeZone : '" + TimeZone + "', Offset : '" + Offset + "'}",
        success: function (data) {
            debugger;
            if (data.indexOf("Invalid") != -1) {
                //alert(data); 
                ShowMessage(data, "no", "Login");
            }
            else {
                //alert(data);
                if (document.getElementById('login-remember-me').checked) {
                    CreateCookie('remember', 'yes', 365);
                    CreateCookie('u', $('#txtemail').val(), 365);
                    CreateCookie('p', $('#txtpassword').val(), 365);
                    
                }
                else {
                    CreateCookie('remember', 'no', 365);
                    CreateCookie('u', '', 365);
                    CreateCookie('p', '', 365);
                    
                }

                var currentURL = window.location;
                //window.location = "/Dashboard/Dashboard/Welcome";

                var redirect = getParameterByName('redirect');
                if (redirect != null) {
                    window.location.href =  redirect;
                }
                else {
                    window.location.href = "/Dashboard/Dashboard/";
                }

            }

            $("#hidein5sec").css('display', 'none');
            $("#hidein5sec").fadeOut();


        },
        error: function (error) {
            //alert(error);
            ShowMessage(error, "no", "Login");
            $("#hidein5sec").css('display', 'none');
            $("#hidein5sec").fadeOut();
        }
    });

}
function CallOffice365Login() {
    $("#hidein5sec").css('display', 'block');
    $("#hidein5sec").fadeIn();

    var localTime = new Date();
    var Offset = createOffset(localTime);
    var TimeZone = localTime;

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: "/Login/CheckOffice365Login",
        data: "{email : '" + $('#txtemail').val() + "', password : '" + $('#txtpassword').val() + "', TimeZone : '" + TimeZone + "', Offset : '" + Offset + "'}",
        success: function (data) {
            debugger;
            if (data.Result != "OK") {
                //alert(data); 
                ShowMessage(data.Result, "no", "Login");
            }
            else {
                //alert(data);
                if (document.getElementById('login-remember-me').checked) {
                    CreateCookie('remember', 'yes', 365);
                    CreateCookie('u', $('#txtemail').val(), 365);
                    CreateCookie('p', $('#txtpassword').val(), 365);
                }
                else {
                    CreateCookie('remember', 'no', 365);
                    CreateCookie('u', '', 365);
                    CreateCookie('p', '', 365);
                }

                var currentURL = window.location;
                window.location = "/Dashboard/Dashboard";
            }

            $("#hidein5sec").css('display', 'none');
            $("#hidein5sec").fadeOut();


        },
        error: function (error) {
            //alert(error);
            ShowMessage(error, "no", "Login");
            $("#hidein5sec").css('display', 'none');
            $("#hidein5sec").fadeOut();
        }
    });

}

function pad(value) {
    return value < 10 ? '0' + value : value;
}
function createOffset(date) {
    var sign = (date.getTimezoneOffset() > 0) ? "-" : "+";
    var offset = Math.abs(date.getTimezoneOffset());
    var hours = pad(Math.floor(offset / 60));
    var minutes = pad(offset % 60);
    return sign + hours + ":" + minutes;
}