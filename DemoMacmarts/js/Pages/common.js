﻿function ModalPopup(ModuleName, IsSuccessMsg, IsInfoMsg, IsWarningMsg, IsErrorMsg, Message, isURL) {
    $('#divErrorMsg').modal();
    if (IsSuccessMsg.toLowerCase() == "yes") {
        $('#modalHeader').removeClass();
        $('#divModalContainer').removeClass();
        $('#btnModalOk').removeClass();
        $('#modalHeader').addClass("top_header_popup success");
        $('#modalHeader').html("<h1><i class='fa fa-thumbs-up'></i>&nbsp;&nbsp;" + ModuleName + "</h1>");
        $('#divModalContainer').addClass("button_contaner success-btn");
        $('#divModalFooterOk').css('display', 'block');
        $('#divModalFooterYesNo').css('display', 'none');
        $('#btnModalOk').addClass("btn btn-effect-ripple btn-success");
        debugger;
        if (isURL != '') {
            $("#btnModalOk").click(function () {
                window.location = isURL;
            });
        }
        $('#lblErrorMsg').html(Message);
    }
    else if (IsInfoMsg.toLowerCase() == "yes") {
        $('#modalHeader').removeClass();
        $('#divModalContainer').removeClass();
        $('#btnModalOk').removeClass();
        $('#modalHeader').addClass("top_header_popup info");
        $('#modalHeader').html("<h1><i class='fa fa-info-circle'></i>&nbsp;&nbsp;" + ModuleName + "</h1>");
        $('#divModalContainer').addClass("button_contaner info-btn");
        $('#divModalFooterOk').css('display', 'block');
        $('#divModalFooterYesNo').css('display', 'none');
        $('#btnModalOk').addClass("btn btn-effect-ripple btn-info");
        $('#lblErrorMsg').html(Message);
    }
    else if (IsWarningMsg.toLowerCase() == "yes") {
        $('#modalHeader').removeClass();
        $('#divModalContainer').removeClass();
        $('#btnModalOk').removeClass();
        $('#modalHeader').addClass("top_header_popup warning");
        $('#modalHeader').html("<h1><i class='fa fa-warning'></i>&nbsp;&nbsp;" + ModuleName + "</h1>");
        $('#divModalContainer').addClass("button_contaner warning-btn");
        $('#divModalFooterOk').css('display', 'none');
        $('#divModalFooterYesNo').css('display', 'block');
        $('#btnModalYes').addClass("btn btn-effect-ripple btn-warning");
        $('#btnModalNo').addClass("btn btn-effect-ripple btn-NoCss");
        $('#lblErrorMsg').html(Message);
    }
    else if (IsErrorMsg.toLowerCase() == "yes") {
        $('#modalHeader').removeClass();
        $('#divModalContainer').removeClass();
        $('#btnModalOk').removeClass();
        $('#modalHeader').addClass("top_header_popup error");
        $('#modalHeader').html("<h1><i class='fa fa-minus-circle'></i>&nbsp;&nbsp;" + ModuleName + "</h1>");
        $('#divModalContainer').addClass("button_contaner error-btn");
        $('#divModalFooterOk').css('display', 'block');
        $('#divModalFooterYesNo').css('display', 'none');
        $('#btnModalOk').addClass("btn btn-effect-ripple btn-danger");
        $('#lblErrorMsg').html(Message);
    }
    else { }
}
function ShowMessage(str, isSuccess, PageName, isUrl, IsWarningMsg) {
    var success = "no";
    var err = "no";

    if (isSuccess == "yes") {
        success = "yes";
    }
    else {
        err = "yes";
    }
    if (isUrl == undefined) {
        isUrl = '';
    }
    IsWarningMsg = IsWarningMsg == undefined ? "no" : IsWarningMsg;

    ModalPopup(PageName, success, "no", IsWarningMsg, err, str, isUrl);

}

var $loading = $('#loadingDiv').hide();
$(document)
  .ajaxStart(function () {
      debugger;
      if (loadermsg == "") {
          loadermsg = "<strong>Loading...</strong>";
      }
      $('#divloadermsg').html(loadermsg)
      $loading.show();
  })
  .ajaxStop(function () {
      $loading.hide();
  })
.error(function () {
    $loading.hide();
});

function OpenCommentsPopUp(id) {
    $('#comment').modal({ backdrop: 'static', keyboard: false, })
    document.getElementById('hidrequisitionid').value = id;
    debugger;
    $('#comment').load('/RFQ/getNewComments/' + id, function (data) {//getComments
        $('#comment').html(data);
    });
}
function OpenReplyBox(role, CommentedToid, CommentedToCompanyID) {

    debugger;
    if (role == "M") {
        $('#divreplyingto').html('Replying to Macnovate');
    }
    else if (role == "C") {
        $('#divreplyingto').html('Replying to Client');
    }
    else if (role == "V") {
        $('#divreplyingto').html('Replying to Vendor');
    }
    $('#txtReplyingToid').val(CommentedToid);
    $('#txtReplyingTo').val(role);
    $('#txtReplyingToCID').val(CommentedToCompanyID);
    $('#txtcomments').val('');
    $('#open_comm').show();
}


function CloseReplyBox() {
    $('#divreplyingto').html('');
    $('#txtReplyingTo').val('');
    $('#open_comm').hide();
    $('#txtcomments').val('');
    $('#txtReplyingToid').val('');
    $('#txtReplyingToCID').val('');
}
function Reply() {
    debugger;
    var comment = $('#txtcomments').val();
    var replyingto = $('#txtReplyingTo').val();
    var CommentedToid = $('#txtReplyingToid').val();
    var ReplyingToCID = $('#txtReplyingToCID').val();

    var id = document.getElementById('hidrequisitionid').value;
    if (comment == "") {
        ShowMessage('Please add comment', "no", "Comment");
    }
    else {
        jQuery.ajax({
            url: '/RFQ/ReplyComment',
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ comment: comment, replyingto: replyingto, id: id, CommentedToid: CommentedToid, ReplyingToCID: ReplyingToCID }),
            success: function (data) {
                debugger;
                if (data == "success") {
                    ShowMessage('Comment submitted successfully', "yes", "Project Tracker");
                    $('#comment').modal('hide');
                    return false;
                }
            },
            error: function (xhr) {
                debugger;

            }
        });
    }

}
function Reply1(t, ReplyingToCID) {
    debugger;

    var comment = '';
    var replyingto = '';
    var CommentedToid = '';
    if (t == 'C') {
        comment = $('#txtcomments_C').val();
        replyingto = $('#txtReplyingTo_C').val();
        CommentedToid = $('#txtReplyingToid_C').val();
    }
    else if (t == 'V') {
        comment = $('#txtcomments_V').val();
        replyingto = $('#txtReplyingTo_V').val();
        CommentedToid = $('#txtReplyingToid_V').val();
    }


    var id = document.getElementById('hidrequisitionid').value;
    if (comment == "") {
        ShowMessage('Please add comment', "no", "Comment");
    }
    else {
        jQuery.ajax({
            url: '/RFQ/ReplyComment',
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ comment: comment, replyingto: replyingto, id: id, CommentedToid: CommentedToid, ReplyingToCID: ReplyingToCID }),
            success: function (data) {
                debugger;
                if (data == "success") {
                    ShowMessage('Comment submitted successfully', "yes", "Project Tracker");
                    $('#comment').modal('hide');
                    return false;
                }
            },
            error: function (xhr) {
                debugger;

            }
        });
    }

}
function ReplyToVendor(replyingto, CommentedToid) {
    debugger;
    var comment = $('#txtcomments_' + CommentedToid).val();
    var id = document.getElementById('hidrequisitionid').value;
    if (comment == "") {
        ShowMessage('Please add comment', "no", "Comment");
    }
    else {
        jQuery.ajax({
            url: '/RFQ/ReplyToVendor',
            type: "POST",
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({ comment: comment, replyingto: replyingto, id: id, CommentedToid: CommentedToid }),
            success: function (data) {
                debugger;
                if (data == "success") {
                    ShowMessage('Comment submitted successfully', "yes", "Project Tracker");
                    $('#comment').modal('hide');
                    return false;
                }
            },
            error: function (xhr) {
                debugger;

            }
        });
    }
}

//----------GET CURRENT DATE AND FUTURED ONE YEAR DATE

function GetCurrentDate() {
    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var output = (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + d.getFullYear();
    return output;
}
function GetAfterOneYearDate() {

    var d = new Date();
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = (d.getFullYear() + 1);

    var output = (day < 10 ? '0' : '') + day + '/' + (month < 10 ? '0' : '') + month + '/' + year;
    return output;

}



//--------CHECK FILE EXTENSION


///--Check File Extension
function FileExtCheck(filename, extn) {
    debugger;
    var ext = filename.split('.').pop().toLowerCase();
    if (extn == "IMAGE") {

        if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
            ShowMessage('Please upload image file', "no", "Vendor Master");
            $("#ProfilePic").val('');
            return false;
        } else
            return true;
    }
    else if (extn == "PDFAUDIT") {
        if ($.inArray(ext, ['pdf']) == -1) {
            ShowMessage('Please upload pdf file', "no", "Vendor Master");
            $("#AuditReport").val('');
            return false;
        }
        else
            return true;

    }
    else if (extn == "PDFNDA") {
        if ($.inArray(ext, ['pdf']) == -1) {
            ShowMessage('Please upload pdf file', "no", "Vendor Master");
            $("#NDAFileUpload").val('');
            return false;
        } else
            return true;

    }
    else
        return true;
}


$("#Phone1,#Phone2").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message

        return false;
    }
});

function numericFilter(txb) {
    txb.value = txb.value.replace(/[^\0-9]/ig, ""); //alert(txb.id);
}
function numericFilter1(txb) {
    debugger;
    txb.value = txb.value.replace(/[^\0-9]/ig, "");
    if (txb.value != "") {
        var id = txb.id.split('_');
        var quantity = document.getElementById('txtitemqty_' + id[1]).value;
        var total = parseFloat(txb.value) * parseFloat(quantity);
        document.getElementById('txtitemctrate_' + id[1]).value = total;

    }
}

function numericFilter3(txb) {
    debugger;
    txb.value = txb.value.replace(/[^\0-9]/ig, "");
    if (txb.value != "") {
        var id = txb.id.split('_');
        var quantity = document.getElementById('txtitemqty_' + id[1]).value;
        var total = parseFloat(txb.value) * parseFloat(quantity);
        document.getElementById('txtitemmtrate_' + id[1]).value = total;

    }
}

////------------CALL INACTIVE BUTTON 
//    $("#btnModalYes").bind('click', function () {
//        var id = $("#hdnPrimaryId").val();
//        var pageName=$("#hdnPageName").val();
//        var isActive = Boolean($("#hdnIsActive").val());
//        debugger;
//        if(pageName=='MacnovateMaster'){
//            $.post('/MacnovateMaster/UserStatus', { Id: id, IsActive: isActive }, function (data) {

//                    if (data.Result == "SUCCESS") {
//                        ShowMessage('User status changed successfully', "yes", "Macnovate master", "", "no");
//                    }
//                    if (data.Result == "TFAIL") {
//                        ShowMessage('Please change mapping of Admin User to other SuperAdmin Role', "no", "Macnovate Master");
//                    }
//                    if (data.Result == "AFAIL") {
//                        ShowMessage('Please change mapping of Local to other Admin Role', "no", "Macnovate Master");
//                    }

//                    else {
//                        ShowMessage('Something wrong happened. Please contact administrator', "no", "Macnovate Master");
//                    }
//                });
//            }
//    });