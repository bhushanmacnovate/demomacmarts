﻿
var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

$(document).ready(function () {

    var maxImageWidth = 250, maxImageHeight = 100;

    $("#divdocuments").dropzone({
        maxFiles: 1,
        dictDefaultMessage: "Drop files here to upload OR Click here",
        addRemoveLinks: true,
        init: function()
        {
            this.on("thumbnail", function (file) {
                // Do the dimension checks you want to do
                if (file.width > maxImageWidth || file.height > maxImageHeight) {
                    file.rejectDimensions()
                }
                else {
                    file.acceptDimensions();
                }
            });

            //this.on("removedfile", function (file) {
            //    //Remove from server
            //    //document.getElementById('hreflogo').style.display = 'none';
            //    //$("#hreflogo").removeAttr("href");
            //    //$('#Logo').val("");
            //});

            this.on("maxfilesexceeded", function(file){
                ShowMessage("You cannot upload more than 1 file.","no","Logo");
            });
        },
        accept: function (file, done) {
            console.log(file);
            if ((file.type).toLowerCase() != "image/jpg" &&
                    (file.type).toLowerCase() != "image/gif" &&
                    (file.type).toLowerCase() != "image/jpeg" &&
                    (file.type).toLowerCase() != "image/png"
                    ) {
                ShowMessage('Please upload image file only.', "no", "Logo");
                done("Invalid file");
            }
            else {
                file.rejectDimensions = function ()
                {
                    ShowMessage('Please upload image only by 250*100 pixel in size.', "no", "Logo");
                    done("Invalid dimension.");
                };
                //done();
                file.acceptDimensions = done;
            }

        },
        url: "/MasterValue/MasterValue/UploadLogo",
        success: function (file, response) {
            debugger;
            if (response.Result == "OK")
            {
                $("#divlogo").load('/MasterValue/MasterValue/LoadAllLogo', function (data) {
                    debugger;
                    $("#divlogo").html(data);
                    ShowMessage('Logo Added Successfully', "yes", "Logo");
                });
            }
            else
            {
                ShowMessage(response.Result, "no", "Logo");
            }

            console.log(response);
        },
        maxfilesexceeded :function(file)
        {
            this.removeFile(file);
        }
        //,removedfile: function (file, serverFileName) {
        //    //alert(file.name);
        //}
    });

    //$("#divdocuments").dropzone({
    //    maxFiles: 1,
    //    addRemoveLinks: true,
    //    dictDefaultMessage: "Drop files here to upload OR Click here",
    //    acceptedFiles: ".png,.jpg,.jpeg",
    //    init: function () {
    //        this.on("removedfile", function (file) {
    //            //Remove from server
    //            document.getElementById('hrefdocuments').style.display = 'none';
    //            $("#hrefdocuments").removeAttr("href");
    //            //$('#DocumentFileName').val("");
    //            //$('#OrgDocumentFileName').val("");
    //        });
    //         this.on("maxfilesexceeded", function(file){
    //            ShowMessage("Please save uploded file first.","no","File Upload");
    //        });
    //    },
    //    url: "/MasterValue/MasterValue/UploadLogo",
    //    success: function (file, response) {
    //        debugger;
    //        if (response.Result == "OK") {
    //            //document.getElementById('hrefdocuments').style.display = 'block';
    //            //$("#hrefdocuments").removeAttr("href");
    //            //$("#hrefdocuments").attr('href', 'http://upload.macmarts.com/Upload/docs/pdf/' + response.FileName);

    //            //$('#DocumentFileName').val(response.FileName);
    //            //$('#OrgDocumentFileName').val(response.OriginalFileName);
    //              $("#divlogo").load('/MasterValue/MasterValue/LoadAllLogo', function (data) {
    //                debugger;
    //                $("#divlogo").html(data);
    //                ShowMessage('Logo Added Successfully', "yes", "Logo");
    //            });
    //        }
    //        else {
    //            ShowMessage(response.Result, "no", "Logo");
    //        }
    //        console.log(response);
    //    }
    //});
});



$("#ddlCurrency").on('change', function () {
    debugger;
    var Currency=$("#ddlCurrency").val();

    $.post('/MasterValue/MasterValue/UpdateCurrency', { Currency: Currency }, function (data) {
        debugger;
        if (data.Result == "OK") {
            ShowMessage('Currency saved Successfully', "yes", "Currency Update");
        }
        else { ShowMessage(data.Result, "no", "Currency Update"); }

    });
});

function DeleteLogo(id)
{
    if (confirm('Are you sure,want to delete the logo?')==true)
    {
        $.post('/MasterValue/MasterValue/DeleteLogo', { id: id }, function (data) {
            debugger;
            if (data.Result == "OK") {
                $("#divlogo").load('/MasterValue/MasterValue/LoadAllLogo', function (data) {
                    debugger;
                    $("#divlogo").html(data);
                    ShowMessage('Logo deleted Successfully', "yes", "Logo");
                });
            }
            else { ShowMessage(data.Result, "no", "Currency Update"); }

        });
    }

}

function savedetails()
{
    debugger;

    var PaymentTerms = document.getElementById("PaymentTerms").value;
    if ($('#CustomerName').val() == "") {
        ShowMessage('Please enter Company Name', "no", "Master Value");
        $('#CustomerName').focus();
        return false;
    }
    else if (!emailReg.test($('#CustomerEmail').val()) && $('#CustomerEmail').val()!="") {
        ShowMessage('Please enter valid Company Email Id', "no", "Master Value");
        $('#CustomerEmail').focus();
        return false;
    }
    else if (!emailReg.test($('#SiteAdminEmail1').val()) && $('#SiteAdminEmail1').val() != "") {
        ShowMessage('Please enter valid Site Admin Email1', "no", "Master Value");
        $('#SiteAdminEmail1').focus();
        return false;
    }
    else if (!emailReg.test($('#SiteAdminEmail2').val()) && $('#SiteAdminEmail2').val() != "") {
        ShowMessage('Please enter valid Site Admin Email2', "no", "Master Value");
        $('#SiteAdminEmail2').focus();
        return false;
    }
    else if (!emailReg.test($('#SiteAdminEmail3').val()) && $('#SiteAdminEmail3').val() != "") {
        ShowMessage('Please enter valid Site Admin Email3', "no", "Master Value");
        $('#SiteAdminEmail3').focus();
        return false;
    }
    else if ($('#SiteAdminEmail1').val() == "" && $('#SiteAdminEmail2').val() == "" && $('#SiteAdminEmail3').val() == "") {
        ShowMessage('Please enter atleast one site admin Email Id', "no", "Master Value");
        
        return false;
    }
    else
    {
        var _IPModel = {};

        _IPModel.ShippingAddressLine1 = $("#ShippingAddressLine1").val();
        _IPModel.ShippingAddressLine2 = $("#ShippingAddressLine2").val();
        _IPModel.ShippingAddressLine3 = $("#ShippingAddressLine3").val();

        _IPModel.CustomerName = $("#CustomerName").val();
        _IPModel.CustomerEmail = $("#CustomerEmail").val();
        _IPModel.CustomerPhone = $("#CustomerPhone").val();

        _IPModel.CustomerAddressLine1 = $("#CustomerAddressLine1").val();
        _IPModel.CustomerAddressLine2 = $("#CustomerAddressLine2").val();
        _IPModel.CustomerAddressLine3 = $("#CustomerAddressLine3").val();

        _IPModel.SiteAdminEmail1 = $("#SiteAdminEmail1").val();
        _IPModel.SiteAdminEmail2 = $("#SiteAdminEmail2").val();
        _IPModel.SiteAdminEmail3 = $("#SiteAdminEmail3").val();

        _IPModel.SiteAdminName1 = $("#SiteAdminName1").val();
        _IPModel.SiteAdminName2 = $("#SiteAdminName2").val();
        _IPModel.SiteAdminName3 = $("#SiteAdminName3").val();

        _IPModel.BankName = $("#BankName").val();
        _IPModel.BankAddress = $("#BankAddress").val();
        _IPModel.IBAN = $("#IBAN").val();

        _IPModel.VAT_TAX = $("#VAT_TAX").val();
        _IPModel.AccountNumber = $("#AccountNumber").val();
        _IPModel.CustomerRegNo = $("#CustomerRegNo").val();

        _IPModel.POTerms = $("#POTerms").val();
        _IPModel.QuotationTerms = $("#QuotationTerms").val();
        _IPModel.SITerms = $("#SITerms").val();
        _IPModel.PaymentTerms = $("#PaymentTerms").val();
        var pTmodel = JSON.stringify(_IPModel);

        $.post('/MasterValue/MasterValue/SaveDetails', { pTmodel: pTmodel }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Details saved successfully.You need to logout and login again to reflect the changes.', "yes", "Master Values", '/Logout/Index');
            }
            else { ShowMessage(data.Result, "no", "Master Values"); }

        });
    }
    
}
