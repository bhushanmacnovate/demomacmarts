﻿function validateCustomerSubscription()
{
    debugger;

    var Today = new Date();
    var dd = Today.getDate();
    var mm = Today.getMonth() + 1; //January is 0!
    var yyyy = Today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var Today = dd + '/' + mm + '/' + yyyy;

    currentdate = Today.split('/');
    var currentDateValue = new Date(currentdate[2], currentdate[1], currentdate[0]);

    var StartDate = $.trim($("[id$='StartDate']").val());
    var StartDatevalue = currentDateValue;
    if (StartDate != '') {
        dateStartDate = StartDate.split('/');
        StartDatevalue = new Date(dateStartDate[2], dateStartDate[1], dateStartDate[0]);
    }

    var EndDate = $.trim($("[id$='EndDate']").val());
    var EndDatevalue = currentDateValue;
    if (EndDate != '') {
        dateEndDate = EndDate.split('/');
        EndDatevalue = new Date(dateEndDate[2], dateEndDate[1], dateEndDate[0]);
    }

    var ReminderDate = $.trim($("[id$='ReminderDate']").val());
    var ReminderDatevalue = currentDateValue;
    if (ReminderDate != '') {
        dateReminderDate = ReminderDate.split('/');
        ReminderDatevalue = new Date(dateReminderDate[2], dateReminderDate[1], dateReminderDate[0]);
    }

    if ($('#ClientId').val() == '') {
        ShowMessage('Please select Customer', "no", "Customer Subscription");
        $('#ClientId').focus();
        return false;
    }
    else if ($('#SubScriptionTypeId').val() == '') {
        ShowMessage('Please select Subscription Type', "no", "Customer Subscription");
        $('#SubScriptionTypeId').focus();
        return false;
    }
    else if ($('#StartDate').val() == '') {
        ShowMessage('Please Select Start Date', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if (StartDatevalue < currentDateValue) {
        ShowMessage('Start Date should be greater than today', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#EndDate').val() == '') {
        ShowMessage('Please Select End Date', "no", "Customer Subscription");
        //$('#EndDate').focus();
        return false;
    }
    else if (EndDatevalue < StartDatevalue) {
        ShowMessage('End Date should be greater than Start Date', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#ReminderDate').val() == '') {
        ShowMessage('Please Select Reminder Date', "no", "Customer Subscription");
        //$('#ReminderDate').focus();
        return false;
    }
    else if (ReminderDatevalue > EndDatevalue) {
        ShowMessage('Reminder Date should be less than End Date', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else
    {
        var _ptModel = {};
        _ptModel.ClientId = $("#ClientId").val();
        _ptModel.SubScriptionTypeId = $("#SubScriptionTypeId").val();
        _ptModel.StartDate = $("#StartDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.EndDate = $("#EndDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.ReminderDate = $("#ReminderDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.Comment = $("#Comment").val();       
        var model = JSON.stringify(_ptModel);

        $.post('/Subscription/Customer/AddSubscription', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Subscription Added Successfully', "yes", "Customer Subscription", "/Subscription/Customer");
            }
            else { ShowMessage(data.Result, "no", "Customer Subscription"); }

        });
    }
}
function validateUpdateCustomerSubscription() {
    debugger;

    var Today = new Date();
    var dd = Today.getDate();
    var mm = Today.getMonth() + 1; //January is 0!
    var yyyy = Today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var Today = dd + '/' + mm + '/' + yyyy;

    currentdate = Today.split('/');
    var currentDateValue = new Date(currentdate[2], currentdate[1], currentdate[0]);

    var StartDate = $.trim($("[id$='StartDate']").val());
    var StartDatevalue = currentDateValue;
    if (StartDate != '') {
        dateStartDate = StartDate.split('/');
        StartDatevalue = new Date(dateStartDate[2], dateStartDate[1], dateStartDate[0]);
    }

    var EndDate = $.trim($("[id$='EndDate']").val());
    var EndDatevalue = currentDateValue;
    if (EndDate != '') {
        dateEndDate = EndDate.split('/');
        EndDatevalue = new Date(dateEndDate[2], dateEndDate[1], dateEndDate[0]);
    }

    var ReminderDate = $.trim($("[id$='ReminderDate']").val());
    var ReminderDatevalue = currentDateValue;
    if (ReminderDate != '') {
        dateReminderDate = ReminderDate.split('/');
        ReminderDatevalue = new Date(dateReminderDate[2], dateReminderDate[1], dateReminderDate[0]);
    }

    if ($('#ClientId').val() == '') {
        ShowMessage('Please select Customer', "no", "Customer Subscription");
        $('#ClientId').focus();
        return false;
    }
    else if ($('#SubScriptionTypeId').val() == '') {
        ShowMessage('Please select Subscription Type', "no", "Customer Subscription");
        $('#SubScriptionTypeId').focus();
        return false;
    }
    else if ($('#StartDate').val() == '') {
        ShowMessage('Please Select Start Date', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if (StartDatevalue < currentDateValue) {
        ShowMessage('Start Date should be greater than today', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#EndDate').val() == '') {
        ShowMessage('Please Select End Date', "no", "Customer Subscription");
        //$('#EndDate').focus();
        return false;
    }
    else if (EndDatevalue < StartDatevalue) {
        ShowMessage('End Date should be greater than Start Date', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#ReminderDate').val() == '') {
        ShowMessage('Please Select Reminder Date', "no", "Customer Subscription");
        //$('#ReminderDate').focus();
        return false;
    }
    else if (ReminderDatevalue > EndDatevalue) {
        ShowMessage('Reminder Date should be less than End Date', "no", "Customer Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else {
        var _ptModel = {};
        _ptModel.ClientId = $("#ClientId").val();
        _ptModel.SubScriptionTypeId = $("#SubScriptionTypeId").val();
        _ptModel.StartDate = $("#StartDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.EndDate = $("#EndDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.ReminderDate = $("#ReminderDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.Comment = $("#Comment").val();
        var model = JSON.stringify(_ptModel);

        $.post('/Subscription/Customer/UpdateSubscription', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Subscription saved Successfully', "yes", "Customer Subscription", "/Subscription/Customer");
            }
            else { ShowMessage(data.Result, "no", "Customer Subscription"); }

        });
    }
}
function cancelCustomerSubscription()
{
    window.location = '/Subscription/Customer';
}

function validateSupplierSubscription() {
    debugger;

    var Today = new Date();
    var dd = Today.getDate();
    var mm = Today.getMonth() + 1; //January is 0!
    var yyyy = Today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var Today = dd + '/' + mm + '/' + yyyy;

    currentdate = Today.split('/');
    var currentDateValue = new Date(currentdate[2], currentdate[1], currentdate[0]);

    var StartDate = $.trim($("[id$='StartDate']").val());
    var StartDatevalue = currentDateValue;
    if (StartDate != '') {
        dateStartDate = StartDate.split('/');
        StartDatevalue = new Date(dateStartDate[2], dateStartDate[1], dateStartDate[0]);
    }

    var EndDate = $.trim($("[id$='EndDate']").val());
    var EndDatevalue = currentDateValue;
    if (EndDate != '') {
        dateEndDate = EndDate.split('/');
        EndDatevalue = new Date(dateEndDate[2], dateEndDate[1], dateEndDate[0]);
    }

    var ReminderDate = $.trim($("[id$='ReminderDate']").val());
    var ReminderDatevalue = currentDateValue;
    if (ReminderDate != '') {
        dateReminderDate = ReminderDate.split('/');
        ReminderDatevalue = new Date(dateReminderDate[2], dateReminderDate[1], dateReminderDate[0]);
    }

    if ($('#VendorId').val() == '') {
        ShowMessage('Please select Supplier', "no", "Supplier Subscription");
        $('#VendorId').focus();
        return false;
    }
    else if ($('#SubScriptionTypeId').val() == '') {
        ShowMessage('Please select Subscription Type', "no", "Supplier Subscription");
        $('#SubScriptionTypeId').focus();
        return false;
    }
    else if ($('#StartDate').val() == '') {
        ShowMessage('Please Select Start Date', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if (StartDatevalue < currentDateValue) {
        ShowMessage('Start Date should be greater than today', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#EndDate').val() == '') {
        ShowMessage('Please Select End Date', "no", "Supplier Subscription");
        //$('#EndDate').focus();
        return false;
    }
    else if (EndDatevalue < StartDatevalue) {
        ShowMessage('End Date should be greater than Start Date', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#ReminderDate').val() == '') {
        ShowMessage('Please Select Reminder Date', "no", "Supplier Subscription");
        //$('#ReminderDate').focus();
        return false;
    }
    else if (ReminderDatevalue > EndDatevalue) {
        ShowMessage('Reminder Date should be less than End Date', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else {
        var _ptModel = {};
        _ptModel.VendorId = $("#VendorId").val();
        _ptModel.SubScriptionTypeId = $("#SubScriptionTypeId").val();
        _ptModel.StartDate = $("#StartDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.EndDate = $("#EndDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.ReminderDate = $("#ReminderDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.Comment = $("#Comment").val();
        var model = JSON.stringify(_ptModel);

        $.post('/Subscription/Supplier/AddSubscription', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Subscription Added Successfully', "yes", "Supplier Subscription", "/Subscription/Supplier");
            }
            else { ShowMessage(data.Result, "no", "Supplier Subscription"); }

        });
    }
}
function validateUpdateCustomerSubscription() {
    debugger;

    var Today = new Date();
    var dd = Today.getDate();
    var mm = Today.getMonth() + 1; //January is 0!
    var yyyy = Today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var Today = dd + '/' + mm + '/' + yyyy;

    currentdate = Today.split('/');
    var currentDateValue = new Date(currentdate[2], currentdate[1], currentdate[0]);

    var StartDate = $.trim($("[id$='StartDate']").val());
    var StartDatevalue = currentDateValue;
    if (StartDate != '') {
        dateStartDate = StartDate.split('/');
        StartDatevalue = new Date(dateStartDate[2], dateStartDate[1], dateStartDate[0]);
    }

    var EndDate = $.trim($("[id$='EndDate']").val());
    var EndDatevalue = currentDateValue;
    if (EndDate != '') {
        dateEndDate = EndDate.split('/');
        EndDatevalue = new Date(dateEndDate[2], dateEndDate[1], dateEndDate[0]);
    }

    var ReminderDate = $.trim($("[id$='ReminderDate']").val());
    var ReminderDatevalue = currentDateValue;
    if (ReminderDate != '') {
        dateReminderDate = ReminderDate.split('/');
        ReminderDatevalue = new Date(dateReminderDate[2], dateReminderDate[1], dateReminderDate[0]);
    }

    if ($('#VendorId').val() == '') {
        ShowMessage('Please select Supplier', "no", "Supplier Subscription");
        $('#VendorId').focus();
        return false;
    }
    else if ($('#SubScriptionTypeId').val() == '') {
        ShowMessage('Please select Subscription Type', "no", "Supplier Subscription");
        $('#SubScriptionTypeId').focus();
        return false;
    }
    else if ($('#StartDate').val() == '') {
        ShowMessage('Please Select Start Date', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if (StartDatevalue < currentDateValue) {
        ShowMessage('Start Date should be greater than today', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#EndDate').val() == '') {
        ShowMessage('Please Select End Date', "no", "Supplier Subscription");
        //$('#EndDate').focus();
        return false;
    }
    else if (EndDatevalue < StartDatevalue) {
        ShowMessage('End Date should be greater than Start Date', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else if ($('#ReminderDate').val() == '') {
        ShowMessage('Please Select Reminder Date', "no", "Supplier Subscription");
        //$('#ReminderDate').focus();
        return false;
    }
    else if (ReminderDatevalue > EndDatevalue) {
        ShowMessage('Reminder Date should be less than End Date', "no", "Supplier Subscription");
        //$('#StartDate').focus();
        return false;
    }
    else {
        var _ptModel = {};
        _ptModel.VendorId = $("#VendorId").val();
        _ptModel.SubScriptionTypeId = $("#SubScriptionTypeId").val();
        _ptModel.StartDate = $("#StartDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.EndDate = $("#EndDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.ReminderDate = $("#ReminderDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");;
        _ptModel.Comment = $("#Comment").val();
        var model = JSON.stringify(_ptModel);

        $.post('/Subscription/Supplier/UpdateSubscription', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Subscription saved Successfully', "yes", "Supplier Subscription", "/Subscription/Supplier");
            }
            else { ShowMessage(data.Result, "no", "Supplier Subscription"); }

        });
    }
}
function cancelSupplierSubscription() {
    window.location = '/Subscription/Supplier';
}