﻿function validate() {
    //alert('');
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#CompanyName1').val() == '') {
        ShowMessage('Please enter Company Name 1.', "no", "Suppliers");
        $('#CompanyName1').focus();
        return false;
    }
        //else if ($('#CompanyName2').val() == '') {
        //    ShowMessage('Please enter Company Name 2.', "no", "Suppliers");
        //    $('#CompanyName2').focus();
        //    return false;
        //}
    else if ($('#AddressLine1').val() == '') {
        ShowMessage('Please enter Address Line 1.', "no", "Suppliers");
        $('#AddressLine1').focus();
        return false;
    }
    else if ($('#AddressLine2').val() == '') {
        ShowMessage('Please enter Address Line 2.', "no", "Suppliers");
        $('#AddressLine2').focus();
        return false;
    }
    else if ($('#CountryName').val() == '') {
        ShowMessage('Please enter Country Name.', "no", "Suppliers");
        $('#CountryName').focus();
        return false;
    }
    else if ($('#Phone1').val() == '') {
        ShowMessage('Please enter Phone 1.', "no", "Suppliers");
        $('#Phone1').focus();
        return false;
    }
        //else if(phonenumber($('#Phone1').val())!=true)
        //{
        //    //ShowMessage('Invalid Phone', "no", "Suppliers");
        //    return false;
        //}
    //else if ($('#ContactPerson').val() == '') {
    //    ShowMessage('Please enter Contact Person.', "no", "Suppliers");
    //    $('#ContactPerson').focus();
    //    return false;
    //}
    else if ($('#Email1').val() == '') {
        ShowMessage('Please enter Supplier Email Id.', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
    else if (!emailReg.test($('#Email1').val())) {
        ShowMessage('Please enter valid Email Id', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
    //else if ($('#Logo').val() == '') {
    //    ShowMessage('Please select logo.', "no", "Suppliers");
    //    return false;
    //}
    else {

        var _ptModel = {};
        _ptModel.id = $("#id").val() == null ? "0" : $("#id").val();

        _ptModel.SupplierId = $("#SupplierId").val();
        _ptModel.CompanyName1 = $("#CompanyName1").val();
        _ptModel.CompanyName2 = $("#CompanyName2").val();
        _ptModel.AddressLine1 = $("#AddressLine1").val();
        _ptModel.AddressLine2 = $("#AddressLine2").val();
        _ptModel.PinCode = $("#PinCode").val();
        _ptModel.CountryName = $("#CountryName").val();
        _ptModel.StateName = $("#StateName").val();
        _ptModel.CityName = $("#CityName").val();
        _ptModel.Phone1 = $("#Phone1").val();
        _ptModel.Phone2 = $("#Phone2").val();
        _ptModel.ContactPerson = $("#ContactPerson").val();


        _ptModel.VAT_TAX = $("#VAT_TAX").val();

        _ptModel.Email1 = $("#Email1").val();
        _ptModel.BankName = $("#BankName").val();
        _ptModel.BankBranch = $("#BankBranch").val();
        _ptModel.SwiftCode = $("#SwiftCode").val();

        _ptModel.AccountNumber = $("#AccountNumber").val();
        _ptModel.IBN = $("#IBN").val();

        _ptModel.Logo = $("#Logo").val();
        //_ptModel.AuditReportFile = $("#AuditReportFile").val();
        //_ptModel.NDAFile = $("#NDAFile").val();

        //if ($("#NDAUploadDate").val() != "") {
        //    var NDAUploadDate = $("#NDAUploadDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.NDAUploadDate = NDAUploadDate;
        //}
        //if ($("#NDADueDate").val() != "") {
        //    var NDADueDate = $("#NDADueDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.NDADueDate = NDADueDate;
        //}
        //if ($("#AuditUploadDate").val() != "") {
        //    var AuditUploadDate = $("#AuditUploadDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.AuditUploadDate = AuditUploadDate;
        //}
        //if ($("#AuditDueDate").val() != "") {
        //    var AuditDueDate = $("#AuditDueDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.AuditDueDate = AuditDueDate;
        //}

        _ptModel.isActive = document.getElementById('chkisactive').checked;
        var model = JSON.stringify(_ptModel);

        $.post('/Suppliers/Suppliers/AddNewSupplier', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Supplier Added Successfully', "yes", "Suppliers", "/Suppliers/Suppliers");
            }
            else { ShowMessage(data.Result, "no", "Suppliers"); }

        });
    }
}

function AddSupplierUserNew()
{
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if ($('#UserName').val() == '') {
        ShowMessage('Please enter User Name', "no", "Suppliers");
        $('#UserName').focus();
        return false;
    }
    else if ($('#UserEmail').val() == '') {
        ShowMessage('Please enter User Email Id.', "no", "Suppliers");
        $('#UserEmail').focus();
        return false;
    }
    else if (!emailReg.test($('#UserEmail').val())) {
        ShowMessage('Please enter valid User Email Id', "no", "Suppliers");
        $('#UserEmail').focus();
        return false;
    }
    else if ($('#Password').val() == '') {
        ShowMessage('Please enter Password', "no", "Suppliers");
        $('#Password').focus();
        return false;
    }
    else if ($('#ConfirmPassword').val() == '') {
        ShowMessage('Please enter Confirm Password', "no", "Suppliers");
        $('#ConfirmPassword').focus();
        return false;
    }
    else if ($('#Password').val() != $('#ConfirmPassword').val()) {
        ShowMessage('Password and Confirm Password does not matched.', "no", "Suppliers");
        //$('#Email1').focus();
        return false;
    }
    else
    {
        var _ptModel = {};
        _ptModel.VendorId = $("#id").val() == null ? "0" : $("#id").val();
        _ptModel.UserName = $("#UserName").val();
        _ptModel.UserEmail = $("#UserEmail").val();
        _ptModel.Password = $("#Password").val();

        var model = JSON.stringify(_ptModel);

        $.post('/Suppliers/Suppliers/AddSupplierUserNew', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                $("#divUserList").load('/Suppliers/Suppliers/LoadSupplierUserList?VendorId=' + _ptModel.VendorId , function (data) {
                    debugger;
                    $("#divUserList").html(data);

                    $("#UserName").val('');
                    $("#UserEmail").val('');
                    $("#Password").val('');
                    $("#ConfirmPassword").val('');

                    ShowMessage('User Added Successfully', "yes", "Suppliers");
                });                
            }
            else { ShowMessage(data.Result, "no", "Suppliers"); }

        });
    }
}
//$("#Phone1,#Phone2").keypress(function (e) {
//    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
//        //display error message

//        return false;
//    }
//});

//function phonenumber(inputtxt)  
//{  
//  var phoneno = /^\+?([0-9]{2})\)?[-. ]?([0-9]{4})[-. ]?([0-9]{4})$/;  
//  if((inputtxt.value.match(phoneno)))  
//        {  
//      return true;  
//        }  
//      else  
//        {  
//        ShowMessage('Invalid Phone Number', "no", "Suppliers");
//        return false;  
//        }  
//}  

function Updatevalidate() {
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    var message = 'This action will block all users with this company, are you sure you want to proceed?';


    if ($('#CompanyName1').val() == '') {
        ShowMessage('Please enter Company Name 1.', "no", "Suppliers");
        $('#CompanyName1').focus();
        return false;
    }
        //else if ($('#CompanyName2').val() == '') {
        //    ShowMessage('Please enter Company Name 2.', "no", "Suppliers");
        //    $('#CompanyName2').focus();
        //    return false;
        //}
    else if ($('#AddressLine1').val() == '') {
        ShowMessage('Please enter Address Line 1.', "no", "Suppliers");
        $('#AddressLine1').focus();
        return false;
    }
    else if ($('#AddressLine2').val() == '') {
        ShowMessage('Please enter Address Line 2.', "no", "Suppliers");
        $('#AddressLine2').focus();
        return false;
    }
    else if ($('#CountryId').val() == '') {
        ShowMessage('Please select Country.', "no", "Suppliers");
        $('#CountryId').focus();
        return false;
    }
    else if ($('#Phone1').val() == '') {
        ShowMessage('Please select Phone 1.', "no", "Suppliers");
        $('#Phone1').focus();
        return false;
    }
    else if ($('#Phone1').val() == '') {
        ShowMessage('Please select Phone 1.', "no", "Suppliers");
        $('#Phone1').focus();
        return false;
    }
    else if ($('#Email1').val() == '') {
        ShowMessage('Please enter Email Id.', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
    else if (!emailReg.test($('#Email1').val())) {
        ShowMessage('Please enter valid Email1', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
    else if ($('#CountryName').val() == '') {
        ShowMessage('Please enter Country Name.', "no", "Suppliers");
        $('#CountryName').focus();
        return false;
    }
    //else if ($('#Logo').val() == '') {
    //    ShowMessage('Please select logo.', "no", "Suppliers");
    //    return false;
    //}
    else {

        var _ptModel = {};
        _ptModel.id = $("#id").val() == null ? "0" : $("#id").val();
        _ptModel.SupplierId = $("#SupplierId").val();
        _ptModel.CompanyName1 = $("#CompanyName1").val();
        _ptModel.CompanyName2 = $("#CompanyName2").val();
        _ptModel.AddressLine1 = $("#AddressLine1").val();
        _ptModel.AddressLine2 = $("#AddressLine2").val();
        _ptModel.PinCode = $("#PinCode").val();
        _ptModel.CountryName = $("#CountryName").val();
        _ptModel.StateName = $("#StateName").val();
        _ptModel.CityName = $("#CityName").val();
        _ptModel.Phone1 = $("#Phone1").val();
        _ptModel.Phone2 = $("#Phone2").val();
        _ptModel.ContactPerson = $("#ContactPerson").val();


       // _ptModel.VAT_TAX = $("#VAT_TAX").val();

        _ptModel.Email1 = $("#Email1").val();
        _ptModel.BankName = $("#BankName").val();
        _ptModel.BankBranch = $("#BankBranch").val();
        _ptModel.SwiftCode = $("#SwiftCode").val();

        _ptModel.AccountNumber = $("#AccountNumber").val();
        _ptModel.IBN = $("#IBN").val();

        _ptModel.Logo = $("#Logo").val();
        //_ptModel.AuditReportFile = $("#AuditReportFile").val();
       // _ptModel.NDAFile = $("#NDAFile").val();

        //if ($("#NDAUploadDate").val() != "") {
        //    var NDAUploadDate = $("#NDAUploadDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.NDAUploadDate = NDAUploadDate;
        //}
        //if ($("#NDADueDate").val() != "") {
        //    var NDADueDate = $("#NDADueDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.NDADueDate = NDADueDate;
        //}
        //if ($("#AuditUploadDate").val() != "") {
        //    var AuditUploadDate = $("#AuditUploadDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.AuditUploadDate = AuditUploadDate;
        //}
        //if ($("#AuditDueDate").val() != "") {
        //    var AuditDueDate = $("#AuditDueDate").val().replace(/^(\d{1,2}\/)(\d{1,2}\/)(\d{4})$/, "$2$1$3");
        //    _ptModel.AuditDueDate = AuditDueDate;
        //}
        _ptModel.isActive = document.getElementById('chkisactive').checked;
        var model = JSON.stringify(_ptModel);

        if (document.getElementById('chkisactive').checked == false) {
            if (confirm(message) == true) {
                $.post('/Suppliers/Suppliers/UpdateSupplier', { model: model }, function (data) {

                    if (data.Result == "OK") {
                        debugger;
                        ShowMessage('Supplier saved Successfully', "yes", "Suppliers", "/Suppliers/Suppliers");
                    }
                    else { ShowMessage(data.Result, "no", "Suppliers"); }

                });
            }
        }
        else {
            $.post('/Suppliers/Suppliers/UpdateSupplier', { model: model }, function (data) {

                if (data.Result == "OK") {
                    debugger;
                    ShowMessage('Supplier saved Successfully', "yes", "Suppliers", "/Suppliers/Suppliers");
                }
                else { ShowMessage(data.Result, "no", "Suppliers"); }

            });
        }

    }
}

function cancel() {
    window.location.href = '/Suppliers/Suppliers';
}
function cancelUser() {
    window.location.href = '/Suppliers/Suppliers/SupplierUsers';
}
function EditSupplier(id) {
    debugger;
    window.location.href = '/Suppliers/Suppliers/AddSupplier?id=' + id;
}
function EditSupplierUser(id) {
    debugger;
    window.location.href = '/Suppliers/Suppliers/AddSupplierUser?id=' + id;
}
function validateUser() {
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#VendorId').val() == '') {
        ShowMessage('Please select Supplier.', "no", "Suppliers");
        $('#VendorId').focus();
        return false;
    }
    else if ($('#SupplierRole').val() == '') {
        ShowMessage('Please select Role.', "no", "Suppliers");
        $('#SupplierRole').focus();
        return false;
    }
    else if ($('#FirstName').val() == '') {
        ShowMessage('Please enter FirstName.', "no", "Suppliers");
        $('#FirstName').focus();
        return false;
    }
    else if ($('#LastName').val() == '') {
        ShowMessage('Please enter LastName.', "no", "Suppliers");
        $('#CompanyName2').focus();
        return false;
    }
    else if ($('#Email1').val() == '') {
        ShowMessage('Please enter Email 1.', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
    else if (!emailReg.test($('#Email1').val())) {
        ShowMessage('Please enter valid Email 1', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
        //else if (!emailReg.test($('#Email2').val())) {
        //    ShowMessage('Please enter valid Email 2', "no", "Suppliers");
        //    $('#Email2').focus();
        //    return false;
        //}
        //else if ($('#Phone1').val() == '') {
        //    ShowMessage('Please enter Phone 1.', "no", "Suppliers");
        //    $('#Phone1').focus();
        //    return false;
        //}
    else if ($('#Password').val() == '') {
        ShowMessage('Please enter Password.', "no", "Suppliers");
        $('#Password').focus();
        return false;
    }
    else if ($('#ConfirmPassword').val() == '') {
        ShowMessage('Please enter Confirm Password.', "no", "Suppliers");
        $('#ConfirmPassword').focus();
        return false;
    }
    else if ($('#Password').val() != $('#ConfirmPassword').val()) {
        ShowMessage('Password and Confirm Password does not match.', "no", "Suppliers");
        return false;
    }
    else {

        var _ptModel = {};
        _ptModel.UserId = $("#UserId").val() == null ? "0" : $("#UserId").val();

        _ptModel.VendorId = $("#VendorId").val();
        _ptModel.FirstName = $("#FirstName").val();
        _ptModel.LastName = $("#LastName").val();
        _ptModel.Email1 = $("#Email1").val();
        //_ptModel.Email2 = $("#Email2").val();       
        //_ptModel.Phone1 = $("#Phone1").val();
        //_ptModel.Phone2 = $("#Phone2").val();
        _ptModel.Password = $("#Password").val();
        _ptModel.SupplierRole = $("#SupplierRole").val();
        _ptModel.isActive = document.getElementById('chkisactive').checked;
        var model = JSON.stringify(_ptModel);

        $.post('/Suppliers/Suppliers/AddNewSupplierUser', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Supplier User Added Successfully', "yes", "Suppliers", "/Suppliers/Suppliers/SupplierUsers");
            }
            else { ShowMessage(data.Result, "no", "Suppliers"); }

        });
    }
}
function UpdatevalidateUser() {
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#VendorId').val() == '') {
        ShowMessage('Please select Supplier.', "no", "Suppliers");
        $('#VendorId').focus();
        return false;
    }
    else if ($('#SupplierRole').val() == '') {
        ShowMessage('Please select Role.', "no", "Suppliers");
        $('#SupplierRole').focus();
        return false;
    }
    else if ($('#FirstName').val() == '') {
        ShowMessage('Please enter FirstName.', "no", "Suppliers");
        $('#FirstName').focus();
        return false;
    }
    else if ($('#LastName').val() == '') {
        ShowMessage('Please enter LastName.', "no", "Suppliers");
        $('#CompanyName2').focus();
        return false;
    }
    else if ($('#Email1').val() == '') {
        ShowMessage('Please enter Email 1.', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
    else if (!emailReg.test($('#Email1').val())) {
        ShowMessage('Please enter valid Email 1', "no", "Suppliers");
        $('#Email1').focus();
        return false;
    }
        //else if (!emailReg.test($('#Email2').val())) {
        //    ShowMessage('Please enter valid Email 2', "no", "Suppliers");
        //    $('#Email2').focus();
        //    return false;
        //}
        //else if ($('#Phone1').val() == '') {
        //    ShowMessage('Please enter Phone 1.', "no", "Suppliers");
        //    $('#Phone1').focus();
        //    return false;
        //}
    else if ($('#Password').val() == '') {
        ShowMessage('Please enter Password.', "no", "Suppliers");
        $('#Password').focus();
        return false;
    }
    else if ($('#ConfirmPassword').val() == '') {
        ShowMessage('Please enter Confirm Password.', "no", "Suppliers");
        $('#ConfirmPassword').focus();
        return false;
    }
    else if ($('#Password').val() != $('#ConfirmPassword').val()) {
        ShowMessage('Password and Confirm Password does not match.', "no", "Suppliers");
        return false;
    }
    else {

        var _ptModel = {};
        _ptModel.UserId = $("#UserId").val() == null ? "0" : $("#UserId").val();

        _ptModel.VendorId = $("#VendorId").val();
        _ptModel.FirstName = $("#FirstName").val();
        _ptModel.LastName = $("#LastName").val();
        _ptModel.Email1 = $("#Email1").val();
        //_ptModel.Email2 = $("#Email2").val();
        //_ptModel.Phone1 = $("#Phone1").val();
        //_ptModel.Phone2 = $("#Phone2").val();
        _ptModel.Password = $("#Password").val();
        _ptModel.SupplierRole = $("#SupplierRole").val();
        _ptModel.isActive = document.getElementById('chkisactive').checked;
        var model = JSON.stringify(_ptModel);

        $.post('/Suppliers/Suppliers/UpdateSupplierUser', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('Supplier User Updated Successfully', "yes", "Suppliers", "/Suppliers/Suppliers/SupplierUsers");
            }
            else { ShowMessage(data.Result, "no", "Suppliers"); }

        });
    }
}

