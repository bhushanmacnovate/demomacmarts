﻿$(function () {
    $('#btnconfirm').click(function () {
        CallForgot();
    });
});
function CallForgot() {
    debugger;
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;

    if ($('#txtemail').val().trim() == '') {
        ShowMessage('Please enter Email', "no", "Forgot Password");
        $('#txtemail').focus();
        return false;
    }
    else if (!emailReg.test($('#txtemail').val())) {
        ShowMessage('Please enter valid email', "no", "Forgot Password");
        $('#txtemail').focus();
        return false;
    }
    else {
        debugger;
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/Forgot/SendPassword",
            data: "{email : '" + $('#txtemail').val() + "'}",
            success: function (data) {
                debugger;
                if (data.indexOf("Invalid") != -1) {
                    ShowMessage(data, "no", "Forgot Password");
                }
                else if (data.indexOf("error") != -1) {
                    ShowMessage(data, "no", "Forgot Password");
                }
                else {
                    ShowMessage("We have sent an email to set up a new password to the email id you have submitted. Please check your inbox.", "yes", "Forgot Password", "/login/Index");

                    //var currentURL = window.location;
                    //window.location = "/login/Index";

                }
            },
            error: function (error) {
                ShowMessage(error, "no", "Forgot Password");
            }
        });
    }
}