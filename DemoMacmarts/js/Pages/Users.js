﻿function AddUser() {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if ($('#FirstName').val() == '') {
        ShowMessage('Please enter First Name.', "no", "Users");
        $('#FirstName').focus();
        return false;
    }
    else if ($('#LastName').val() == '') {
        ShowMessage('Please enter Last Name.', "no", "Users");
        $('#LastName').focus();
        return false;
    }
    else if ($('#Phone1').val().trim() == '') {
        ShowMessage('Please enter Phone1', "no", "Users");
        $('#Phone1').focus();
        return false;
    }
    else if ($('#Email1').val().trim() == '') {
        ShowMessage('Please enter Email1', "no", "Users");
        $('#Email1').focus();
        return false;
    }
    else if (!emailReg.test($('#Email1').val())) {
        ShowMessage('Please enter valid Email1', "no", "Users");
        $('#Email1').focus();
        return false;
    }
    else if ($('#ddlUserRole').val() == '0') {
        ShowMessage('Please Select USer Role', "no", "Users");
        $('#ddlUserRole').focus();
        return false;
    }
    else
    {
        var _ptModel = {};
        _ptModel.UserId = $("#UserId").val() == null ? "0" : $("#UserId").val();

        _ptModel.FirstName = $("#FirstName").val();
        _ptModel.LastName = $("#LastName").val();
        _ptModel.Email1 = $("#Email1").val();      
        _ptModel.Phone1 = $("#Phone1").val();
        _ptModel.Phone2 = $("#Phone2").val();
        _ptModel.UserRole = $("#ddlUserRole").val();
       
        var model = JSON.stringify(_ptModel);
        debugger;
        $.post('/Users/Users/CreateUser', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('User added successfully', "yes", "Add User","/Users/Users/"); 
            }
            else { ShowMessage(data.Result, "no", "Add User"); }

        });
    }
}
function UpdateUser() {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if ($('#FirstName').val() == '') {
        ShowMessage('Please enter First Name.', "no", "Users");
        $('#FirstName').focus();
        return false;
    }
    else if ($('#LastName').val() == '') {
        ShowMessage('Please enter Last Name.', "no", "Users");
        $('#LastName').focus();
        return false;
    }
    else if ($('#Phone1').val().trim() == '') {
        ShowMessage('Please enter Phone1', "no", "Users");
        $('#Phone1').focus();
        return false;
    }
    else if ($('#Email1').val().trim() == '') {
        ShowMessage('Please enter Email1', "no", "Users");
        $('#Email1').focus();
        return false;
    }
    else if (!emailReg.test($('#Email1').val())) {
        ShowMessage('Please enter valid Email1', "no", "Users");
        $('#Email1').focus();
        return false;
    }
    else if ($('#ddlUserRole').val() == '0') {
        ShowMessage('Please Select USer Role', "no", "Users");
        $('#ddlUserRole').focus();
        return false;
    }
    else {
        debugger;
        var _ptModel = {};
        _ptModel.UserId = $("#UserId").val() == null ? "0" : $("#UserId").val();

        _ptModel.FirstName = $("#FirstName").val();
        _ptModel.LastName = $("#LastName").val();
        _ptModel.Email1 = $("#Email1").val();
        _ptModel.Phone1 = $("#Phone1").val();
        _ptModel.Phone2 = $("#Phone2").val();
        _ptModel.UserRole = $("#ddlUserRole").val();
        var model = JSON.stringify(_ptModel);

        $.post('/Users/Users/UpdateUser', { model: model }, function (data) {

            if (data.Result == "OK") {
                debugger;
                ShowMessage('User added successfully', "yes", "Add User","/Users/Users/"); 
            }
            else { ShowMessage(data.Result, "no", "Add User"); }

        });
    }
}

$("#Phone1,#Phone2").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message

        return false;
    }
});

function cancelUpdateUser()
{
    window.location = '/Users/Users';
}