﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMacmartsBL.BL.User;
using DemoMacmartsBL.ViewModel;

namespace DemoMacmarts.Areas.Users.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users/Users
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult UserList()
        {
            List<UserVM> objUserList = new List<UserVM>();
            UserBL bl = new UserBL();
            objUserList = bl.GetUserList();
            return View("UserList",objUserList);
        }
        public ActionResult Add()
        {
            UserVM objResult = new UserVM();

            return View("Add", objResult);
        }
        [HttpPost]
        public JsonResult CreateUser(string model)
        {
            UserVM details = Newtonsoft.Json.JsonConvert.DeserializeObject<UserVM>(model);
            UserBL objbl = new UserBL();
            string result = objbl.AddClientUser(details);
            if (result == "success")
            {
                result = "OK";
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Update()
        {
            long UserId = Convert.ToInt64(Request.QueryString["id"]);
            UserBL objBL = new UserBL();
          
            UserVM objResult = new UserVM();
            objResult = objBL.getSingleUser(UserId);
            return View("Add", objResult);
        }
        public JsonResult UpdateUser(string model)
        {
            UserVM details = Newtonsoft.Json.JsonConvert.DeserializeObject<UserVM>(model);
            UserBL objBL = new UserBL();
            string result = objBL.UpdateUser(details);
            if (result == "success")
            {
                result = "OK";
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
    }
}