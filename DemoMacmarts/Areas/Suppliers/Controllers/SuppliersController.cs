﻿using DemoMacmartsBL;
using DemoMacmartsBL.ViewModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DemoMacmarts.Controllers;
using System.IO;
using DemoMacmartsCommon;
using DemoMacmartsBL.BL;
using DemoMacmartsBL.BL.Supplier;
using System.Data;

namespace DemoMacmarts.Areas.Suppliers.Controllers
{
    public class SuppliersController : Controller
    {
        // GET: Suppliers/Suppliers
        public ActionResult Index()
        {
            List<SuppliersVM> objResult = new List<SuppliersVM>();
            SupplierBL objbl = new SupplierBL();
            objResult = objbl.LoadAllSuppliers();
            return View("Suppliers", objResult);
        }
        public JsonResult getSupplierRoles(long Id)
        {
            SupplierBL objbl = new SupplierBL();
            var Result = objbl.getSupplierRoles(Id);
            return Json(new { SupplierRole = Result }, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult DeleteSupplierCurrency(long VendorId, string Currency)
        //{
        //    SupplierBL objbl = new SupplierBL();
        //    string Result = objbl.DeleteSupplierCurrency(VendorId, Currency);
        //    return Json(new { Result = Result }, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult AddSupplierCurrency(long VendorId, string Currency)
        {
            SupplierBL objbl = new SupplierBL();
            string Result = objbl.AddSupplierCurrency(VendorId, Currency);
            return Json(new { Result = Result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddSupplier()
        {
            Guid TempId = Guid.NewGuid();
            Session["Sessionid"] = TempId.ToString();

            CommonCode objCommon = new CommonCode();
           
            ViewBag.Currency = objCommon.getCurrency();

            SuppliersVM obj = new SuppliersVM();
            if (Request.QueryString["id"] != null)
            {
                SupplierBL objbl = new SupplierBL();
                obj = objbl.LoadSingleSupplier(Convert.ToInt64(Request.QueryString["id"]));
            }

            return View("AddSupplier", obj);
          }

        [HttpPost]
        public JsonResult AddNewSupplier(string model)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Converters = new List<JsonConverter> { new BadDateFixJsonConverter() },
                DateParseHandling = DateParseHandling.None
            };
            SuppliersVM details = Newtonsoft.Json.JsonConvert.DeserializeObject<SuppliersVM>(model, settings);
            SupplierBL objbl = new SupplierBL();
            string result = objbl.AddSupplier(details);
            if (result == "success")
            {
                result = "OK";
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadSupplierCurrency(long VendorId)
        {
            SupplierBL objbl = new SupplierBL();
            List<string> SupplierCurrency = objbl.LoadSupplierCurrency(VendorId);

            return PartialView("~/Areas/Suppliers/Views/Suppliers/_SupplierCurrency.cshtml", SupplierCurrency);
        }

        [HttpPost]
        public JsonResult UpdateSupplier(string model)
        {
            JsonSerializerSettings settings = new JsonSerializerSettings
            {
                Converters = new List<JsonConverter> { new BadDateFixJsonConverter() },
                DateParseHandling = DateParseHandling.None
            };

            SuppliersVM details = Newtonsoft.Json.JsonConvert.DeserializeObject<SuppliersVM>(model, settings);
            SupplierBL objbl = new SupplierBL();
            string result = objbl.UpdateSupplier(details);
            if (result == "success")
            {
                result = "OK";
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult DeleteSupplierCurrency(long VendorId, string Currency)
        {
            SupplierBL objbl = new SupplierBL();
            string Result = objbl.DeleteSupplierCurrency(VendorId, Currency);
            return Json(new { Result = Result }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SupplierUsers()
        {
           
                List<SupplierUsersVM> objResult = new List<SupplierUsersVM>();
                SupplierBL objbl = new SupplierBL();
                objResult = objbl.LoadAllSupplierUsers();
                return View("SupplierUsers", objResult);
            }
        public ActionResult AddSupplierUser()
        {

            SupplierUsersVM obj = new SupplierUsersVM();
            SupplierBL objbl = new SupplierBL();
            if (Request.QueryString["id"] != null)
            {
                obj = objbl.LoadSingleSupplierUser(Convert.ToInt64(Request.QueryString["id"]));
            }
            ViewBag.SupplierList = objbl.GetAllSupplierList();
            return View("AddSupplierUser", obj);
        }

        [HttpPost]
        public JsonResult AddNewSupplierUser(string model)
        {
            SupplierUsersVM details = Newtonsoft.Json.JsonConvert.DeserializeObject<SupplierUsersVM>(model);
            SupplierBL objbl = new SupplierBL();
            string result = objbl.AddSupplierUser(details);
            if (result == "success")
            {
                result = "OK";
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateSupplierUser(string model)
        {
            SupplierUsersVM details = Newtonsoft.Json.JsonConvert.DeserializeObject<SupplierUsersVM>(model);
            SupplierBL objbl = new SupplierBL();
            string result = objbl.UpdateSupplierUser(details);
            if (result == "success")
            {
                ViewBag.SupplierList = objbl.GetAllSupplierList();
                result = "OK";
            }
            return Json(new { Result = result }, JsonRequestBehavior.AllowGet);
        }


    }


}


