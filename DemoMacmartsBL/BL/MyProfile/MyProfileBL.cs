﻿using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using DemoMacmartsDL;
using System;
using System.IO;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace DemoMacmartsBL.BL.MyProfile
{
    public class MyProfileBL
    {

        public UserVM GetUserDetails(long _UserID)
        {
            string result = "";
            UserVM _objUserDetails = new UserVM();
            try
            {
                using (var db = new Demo_Macmarts_DemoEntities())
                {
                    _objUserDetails = (from g in db.tblUsers
                                       join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                       where g.UserId == _UserID
                                       select new UserVM
                                       {
                                           UserId = g.UserId,
                                           FirstName = g.FirstName,
                                           LastName = g.LastName,
                                           EmailId = g.EmailId,
                                           AltEmailId = g.AltEmailId,
                                           Phone = g.Phone,
                                           AltPhoneNo = g.AltPhoneNo,
                                           UserType = u.ClientOrVendor,
                                           UserRole = u.UserRole,
                                           ReportingId = g.ReportingId,
                                           UserPic = g.UserPic,
                                           // ClientId = u.ClientVendorId,
                                           // ClientName = UsersVM._ClientName,
                                           // VendorName = UsersVM._VendorName
                                       }).FirstOrDefault();
                }
            }
            catch (Exception err)
            {
                result = "error";
            }
            return _objUserDetails;
        }

        public string UpdateUserProfile(UserVM objuser)
        {
            string result = "";
            try
            {
                using (var db = new Demo_Macmarts_DemoEntities())
                {
                    var res = db.tblUsers.SingleOrDefault(b => b.UserId == objuser.UserId);
                    if (res != null)
                    {
                        res.FirstName = objuser.FirstName;
                        res.LastName = objuser.LastName;
                        res.AltEmailId = objuser.AltEmailId;
                        res.Phone = objuser.Phone;
                        res.AltPhoneNo = objuser.AltPhoneNo;
                        db.SaveChanges();
                    }
                }
                result = "success";
            }
            catch (Exception err)
            {
                // CommonMethods.LogError("MyProfileBL", "UpdateUserProfile", err);
                result = "error";
            }

            return result;
        }

        public string ChangePassword(string oldp, string newp, long UserId)
        {
            string result = "";
            try
            {
                using (var db = new Demo_Macmarts_DemoEntities())
                {
                    var res = db.tblUsers.SingleOrDefault(b => b.UserId == UserId && b.Password == oldp);
                    if (res != null)
                    {
                        res.Password = newp;

                        db.SaveChanges();
                        result = "success";
                    }
                    else
                    {
                        result = "Invalid Old Password";
                    }
                }
            }
            catch (Exception err)
            {
                //CommonMethods.LogError("MyProfileBL", "UpdateUserProfile", err);
                result = "error";
            }

            return result;
        }

    }
}
