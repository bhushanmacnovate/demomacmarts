﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMacmartsDL;
using DemoMacmartsCommon;
using System.Configuration;
using System.Web;

namespace DemoMacmartsBL.BL.Forgot
{
    public class ForgotPassBL
    {
        Demo_Macmarts_DemoEntities obj = new Demo_Macmarts_DemoEntities();
        public string CheckPassword(string email)
        {
            string result = "";
            try
            {
                string password = "";
                string FirstName = "";
                var a = (from g in obj.tblUsers
                         where (g.EmailId == email)
                         select new
                         {
                             UserID = g.UserId,
                             FirstName = g.FirstName
                             
                         }).FirstOrDefault();

                if (a == null)
                {
                    result = "Invalid Email Id";
                }

                else
                {

                    String guid = Guid.NewGuid().ToString();
                    using (var db = new Demo_Macmarts_DemoEntities())
                    {
                        var UserIDs = db.tblForgotPasswords.Where(f => f.UserId == a.UserID).ToList();
                        UserIDs.ForEach(x => x.isActive = false);
                        db.SaveChanges();

                        tblForgotPassword _objtbl = new tblForgotPassword();
                        _objtbl.UserId = a.UserID;
                        _objtbl.ForgotPasswordCode = guid;
                        _objtbl.Validity = DateTime.Now.AddDays(1);
                        _objtbl.isActive = true;
                        db.tblForgotPasswords.Add(_objtbl);
                        db.SaveChanges();


                    }

                    FirstName = a.FirstName;

                    ClsEMailer _objMailer = new ClsEMailer();
                    string body = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/mailer/forgotpassword.html"));
                    body = body.Replace("#YOURNAME#", FirstName).Replace("#URL#", ConfigurationManager.AppSettings["VerifyEmailLink"].ToString() + "Forgot/index?code=" + guid);
                    _objMailer.Subject = "Demo Macmarts Portal – Login credentials";
                    _objMailer.MailReciepientsTo = email;
                    _objMailer.BodyMatter = body;
                    if (_objMailer.SendMail())
                    {
                        result = "success";
                    }
                    else
                    {
                        result = "Error sending mail. Please contact Administartor";
                    }

                }

            }
            catch (Exception err)
            {
                result = "error";
            }

            return result;
        }
        public string CheckResetPassword(string code, string password = null)
        {
            string result = "";
            try
            {
                using (var db = new Demo_Macmarts_DemoEntities())
                {
                    var res = (from g in db.tblForgotPasswords
                               where g.ForgotPasswordCode == code
                               select new
                               {
                                   userid = g.UserId,
                                   isActive = g.isActive,
                                   Validity = g.Validity
                               }).FirstOrDefault();

                    if (res != null)
                    {
                        if (res.isActive == false)
                        {
                            result = "Invalid Link! Please contact administrator.";
                        }
                        else if (res.Validity < DateTime.Now)
                        {
                            result = "Link Expired!";
                        }
                        else
                        {
                            if (password == null)
                            {
                                result = "success";
                            }
                            else
                            {
                                var output = db.tblUsers.SingleOrDefault(b => b.UserId == res.userid);
                                EncryptDecrypt e = new EncryptDecrypt();
                                output.Password = e.Encrypt(password);
                                db.SaveChanges();

                                var UserIDs = db.tblForgotPasswords.Where(f => f.UserId == res.userid).ToList();
                                UserIDs.ForEach(x => x.isActive = false);
                                db.SaveChanges();

                                result = "success";

                            }

                        }
                    }
                    else
                    {
                        result = "Invalid Link! Please contact administrator.";
                    }
                }
            }
            catch (Exception err)
            {
                result = "error";
            }
            return result;
        }
    }
}

