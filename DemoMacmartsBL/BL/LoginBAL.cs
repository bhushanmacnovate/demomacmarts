﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DemoMacmartsDL;
using DemoMacmartsBL.ViewModel;
using System.Data;

namespace DemoMacmartsBL
{
    public class LoginBL
    {
        //long ClientId = Convert.ToInt64(ConfigurationManager.AppSettings["ClientId"]);
        public UserVM CheckLogin(string email, string password)
        {
            string Status = "";
            UserVM objUser = new UserVM();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
            {
               
                    objUser = (from g in db.tblUsers
                               where (g.EmailId == email && g.Password == password)
                               select new UserVM
                               {
                                   UserId = g.UserId,
                                   FirstName = g.FirstName,
                                   LastName = g.LastName

                               }).FirstOrDefault();
                    if (objUser != null)
                    {
                        objUser.status = "Valid";
                    }
                    else
                    {
                        objUser.status = "Invalid";
                    }
                }
   
            }
            catch (Exception e)
            {

            }
            return objUser;
        }
    }
}


