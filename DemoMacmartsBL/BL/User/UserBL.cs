﻿using DemoMacmartsBL.ViewModel;

using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DemoMacmartsBL.BL.User
{
    public class UserBL
    {
        //long ClientId = Convert.ToInt64(ConfigurationManager.AppSettings["ClientId"]);
        public List<UserVM> GetUserList()
        {
            List<UserVM> objUser = new List<UserVM>();
            using (var db = new Demo_Macmarts_DemoEntities())
            {

                
                objUser = (from g in db.tblUsers join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                           where u.ClientOrVendor == "C"
                             select new UserVM
                             {
                                 UserId = g.UserId,
                                 FirstName = g.FirstName,
                                 LastName = g.LastName,
                                 EmailId = g.EmailId,
                                 Phone = g.Phone,
                                 isActive = g.isActive,
                                 UserRole = u.UserRole
                             }).Distinct().ToList();
            }
            return objUser;
        }
        public string AddClientUser(UserVM obj)
        {
            string result = "";
            try
            {
                using (var db = new Demo_Macmarts_DemoEntities())
                {
                    string EmailVal = (from g in db.tblUsers
                                        where g.EmailId == obj.EmailId 
                                        select g.EmailId).FirstOrDefault();
                    string SessionId = Convert.ToString(HttpContext.Current.Session["NewUserSessionid"]);
                    //int RoleCount = db.tblUsersClientVendorRoleMappingTemps.Where(x => x.SessionId == SessionId).Count();


                    if (EmailVal != null)
                    {
                        result = "Email Already exists";
                    }
                    //else if(RoleCount==0)
                    //{
                    //    result = "Please select atleast one Role";
                    //}
                    else
                    {
                        tblUser objtbl = new tblUser();
                        objtbl.FirstName = obj.FirstName;
                        objtbl.LastName = obj.LastName;
                        objtbl.EmailId = obj.EmailId;
                        objtbl.Phone = obj.Phone;
                        //objtbl.Phone2 = obj.Phone2;

                        objtbl.CreatedBy = 1;
                        objtbl.CreatedDate = DateTime.Now.ToUniversalTime();
                        objtbl.isActive = true;
                        objtbl.isDelete = false;
                        objtbl.Password = "123456";
                        objtbl.UserPic = "default.png";

                        //objtbl = obj.UserRole;
                        db.tblUsers.Add(objtbl);
                        db.SaveChanges();

                        long UserId = objtbl.UserId;

                        tblUserClientVendorRoleMapping objtblUserMap = new tblUserClientVendorRoleMapping();
                        objtblUserMap.ClientOrVendor = "C";
                        objtblUserMap.ClientVendorId = 1;
                        objtblUserMap.UserRole = obj.UserRole;
                        objtblUserMap.isActive = true;
                        objtblUserMap.CreatedDate = DateTime.Now.ToUniversalTime();
                        objtblUserMap.CreatedBy = 1;
                        objtblUserMap.userid = UserId;
                        db.tblUserClientVendorRoleMappings.Add(objtblUserMap);
                        db.SaveChanges();
                        result = "success";
                    }
                }
            
            }
            catch (Exception err)
            {
                //CommonMethods.LogError("ClientUsersBL", "AddClientUser", err);
                result = "error";
            }
            return result;
        }
        public UserVM getSingleUser(long UserId)
        {
            UserVM objResult = new UserVM();
            try
            {
                using (var db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblUsers
                                     join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                 where g.UserId == UserId
                                 select new UserVM
                                 {
                                     UserId = g.UserId,
                                     FirstName = g.FirstName,
                                     LastName = g.LastName,
                                     EmailId = g.EmailId,
                                     Phone = g.Phone,
                                     isActive = g.isActive,
                                     UserRole = u.UserRole
                                 }).FirstOrDefault();
                    //objResult.UserRoleMapping = getUserRoles("", UserId);
                }
            }
            catch (Exception err)
            {
               
            }
            return objResult;
        }
        public string UpdateUser(UserVM obj)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string EmailVal = (from g in db.tblUsers
                                        where (g.EmailId == obj.EmailId ) && g.UserId != obj.UserId
                                        select g.EmailId).FirstOrDefault();


                    if (EmailVal != null)
                    {
                        result = "Email Already exists";
                    }
                    else
                    {
                        var res = db.tblUsers.SingleOrDefault(b => b.UserId == obj.UserId);
                        if (res != null)
                        {
                            res.FirstName = obj.FirstName;
                            res.LastName = obj.LastName;
                            res.EmailId = obj.EmailId;
                            res.Phone = obj.Phone;
                            res.ModifiedBy = 1;
                            res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                            res.isActive = obj.isActive;
                            db.SaveChanges();

                            var res1 = db.tblUserClientVendorRoleMappings.SingleOrDefault(b => b.userid == obj.UserId);
                            res1.UserRole = obj.UserRole;
                            res1.ModifiedBy = 1;
                            res1.ModifiedDate = System.DateTime.Now.ToUniversalTime();

                            result = "success";
                        }
                        else
                        {
                            result = "Data not found";
                        }

                    }
                }
            }
            catch (Exception err)
            {
                
                result = "error";
            }
            return result;
        }
    }
}
