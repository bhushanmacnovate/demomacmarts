﻿using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL.BL
{
    public class SupplierJustificationBL
    {
        public string AddSupplierJustification(string SupplierJustification)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string SupplierJustificationVal = (from g in db.tblSupplierSelectionJustificationMasters
                                                       where g.SupplierSelectionJustification == SupplierJustification && g.isDelete == false
                                                       select g.SupplierSelectionJustification).FirstOrDefault();

                    if (SupplierJustificationVal != null)
                    {
                        result = "Supplier Justification Already exists";
                    }
                    else
                    {
                        tblSupplierSelectionJustificationMaster objtbl = new tblSupplierSelectionJustificationMaster();

                        objtbl.SupplierSelectionJustification = SupplierJustification;
                        objtbl.CreatedBy = UserVM._UserID;
                        objtbl.CreatedDate = DateTime.Now.ToUniversalTime();
                        objtbl.isDelete = false;
                        db.tblSupplierSelectionJustificationMasters.Add(objtbl);
                        //db.Entry(objtbl).State = EntityState.Added;
                        db.SaveChanges();
                        result = "success";

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SupplierJustificationBL", "AddSupplierJustification", err);
                result = "error";
            }
            return result;
        }
        public string UpdateSupplierJustification(string SupplierJustification, int id)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string SupplierJustificationVal = (from g in db.tblSupplierSelectionJustificationMasters
                                                       where g.SupplierSelectionJustification == SupplierJustification && g.id != id && g.isDelete == false
                                                       select g.SupplierSelectionJustification).FirstOrDefault();

                    if (SupplierJustificationVal != null)
                    {
                        result = "Supplier Justification Already exists";
                    }
                    else
                    {
                        var res = db.tblSupplierSelectionJustificationMasters.SingleOrDefault(b => b.id == id);
                        if (res != null)
                        {
                            res.SupplierSelectionJustification = SupplierJustification;
                            res.ModifiedBy = UserVM._UserID;
                            res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                            db.SaveChanges();
                            result = "success";
                        }
                        else
                        {
                            result = "Data not found";
                        }

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SupplierJustificationBL", "UpdateSupplierJustification", err);
                result = "error";
            }
            return result;
        }
        public SupplierJustificationListVM LoadAllSupplierJustifications()
        {
            SupplierJustificationListVM objResult = new SupplierJustificationListVM();
            List<SupplierJustificationVM> objData = new List<SupplierJustificationVM>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objData = (from g in db.tblSupplierSelectionJustificationMasters
                               where g.isDelete == false
                               select new SupplierJustificationVM
                               {
                                   id = g.id,
                                   CreatedBy = g.CreatedBy,
                                   CreatedDate = g.CreatedDate,
                                   ModifiedBy = g.ModifiedBy,
                                   ModifiedDate = g.ModifiedDate,
                                   SupplierJustification = g.SupplierSelectionJustification,

                               }).ToList();

                    objResult.SupplierJustificationList = objData;
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SupplierJustificationBL", "LoadAllSupplierJustifications", err);

            }
            return objResult;
        }

        public SupplierJustificationVM LoadSingleSupplierJustification(int id)
        {
            SupplierJustificationVM objResult = new SupplierJustificationVM();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblSupplierSelectionJustificationMasters
                                 where g.id == id
                                 select new SupplierJustificationVM
                                 {
                                     id = g.id,
                                     CreatedBy = g.CreatedBy,
                                     CreatedDate = g.CreatedDate,
                                     ModifiedBy = g.ModifiedBy,
                                     ModifiedDate = g.ModifiedDate,
                                     SupplierJustification = g.SupplierSelectionJustification,

                                 }).FirstOrDefault();


                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SupplierJustificationBL", "LoadSingleSupplierJustification", err);

            }
            return objResult;
        }

        public string DeleteSupplierJustification(int id)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    var res = db.tblSupplierSelectionJustificationMasters.SingleOrDefault(b => b.id == id);
                    if (res != null)
                    {

                        res.isDelete = true;
                        res.ModifiedBy = UserVM._UserID;
                        res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                        db.SaveChanges();
                    }
                    result = "success";
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SupplierJustificationBL", "DeleteSupplierJustification", err);
                result = "error";
            }
            return result;
        }
    }
}

