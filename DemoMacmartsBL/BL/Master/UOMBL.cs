﻿using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DemoMacmartsBL.BL
{
    public class UOMBL
    {
        public string AddUOM(string UOM, string Description)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string UOMVal = (from g in db.tblUnitOfMeasures
                                     where g.Text == UOM && g.isActive == true
                                     select g.Text).FirstOrDefault();

                    if (UOMVal != null)
                    {
                        result = "Unit of measure Already exists";
                    }
                    else
                    {
                        tblUnitOfMeasure objtbl = new tblUnitOfMeasure();
                        objtbl.Text = UOM;
                        objtbl.Value = UOM;
                        objtbl.Description = Description;
                        objtbl.CreatedBy = UserVM._UserID;
                        objtbl.CreatedDate = DateTime.Now.ToUniversalTime();
                        objtbl.isActive = true;
                        db.tblUnitOfMeasures.Add(objtbl);
                        //db.Entry(objtbl).State = EntityState.Added;
                        db.SaveChanges();
                        result = "success";

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("UOMBL", "AddUOM", err);
                result = "error";
            }
            return result;
        }
        public string UpdateUOM(string UOM, string Description, int id)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string UOMVal = (from g in db.tblUnitOfMeasures
                                     where g.Text == UOM && g.id != id && g.isActive == true
                                     select g.Text).FirstOrDefault();

                    if (UOMVal != null)
                    {
                        result = "Unit of measure Already exists";
                    }
                    else
                    {
                        var res = db.tblUnitOfMeasures.SingleOrDefault(b => b.id == id);
                        if (res != null)
                        {
                            res.Text = UOM;
                            res.Value = UOM;
                            res.Description = Description;
                            res.ModifiedBy = UserVM._UserID;
                            res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                            db.SaveChanges();
                            result = "success";
                        }
                        else
                        {
                            result = "Data not found";
                        }

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("UOMBL", "UpdateUOM", err);
                result = "error";
            }
            return result;
        }
        public UOMListVM LoadAllUOMs()
        {
            UOMListVM objResult = new UOMListVM();
            List<UnitOfMeasureVM> objData = new List<UnitOfMeasureVM>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objData = (from g in db.tblUnitOfMeasures
                               where g.isActive == true
                               select new UnitOfMeasureVM
                               {
                                   id = g.id,
                                   CreatedBy = g.CreatedBy,
                                   CreatedDate = g.CreatedDate,
                                   ModifiedBy = g.ModifiedBy,
                                   ModifiedDate = g.ModifiedDate,
                                   Text = g.Text,
                                   Value = g.Value,
                                   Description = g.Description
                               }).ToList();

                    objResult.UOMlist = objData;
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("UOMBL", "LoadUOMs", err);

            }
            return objResult;
        }
        public UnitOfMeasureVM LoadSingleUOM(int id)
        {
            UnitOfMeasureVM objResult = new UnitOfMeasureVM();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblUnitOfMeasures
                                 where g.id == id
                                 select new UnitOfMeasureVM
                                 {
                                     id = g.id,
                                     CreatedBy = g.CreatedBy,
                                     CreatedDate = g.CreatedDate,
                                     ModifiedBy = g.ModifiedBy,
                                     ModifiedDate = g.ModifiedDate,
                                     Text = g.Text,
                                     Value = g.Value,
                                     Description = g.Description
                                 }).FirstOrDefault();


                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("UOMBL", "LoadSingleUOM", err);

            }
            return objResult;
        }

        public string DeleteUOM(int id)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    var res = db.tblUnitOfMeasures.SingleOrDefault(b => b.id == id);
                    if (res != null)
                    {
                        res.isActive = false;
                        res.ModifiedBy = UserVM._UserID;
                        res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                        db.SaveChanges();
                    }
                    result = "success";
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("UOMBL", "DeleteUOM", err);
                result = "error";
            }
            return result;
        }
    }
}
