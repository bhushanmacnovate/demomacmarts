﻿using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL
{
    public class QuestionnaireBL
    {
        public string AddQuestionnair(string QuestionNo, string QuestionText, string QuestionType, bool Inputdata)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string QuestionNoVal = (from g in db.tblQuestions
                                            where g.QuestionNo == QuestionNo && g.isDelete == false
                                            select g.QuestionNo).FirstOrDefault();
                    string QuestionTextVal = (from g in db.tblQuestions
                                              where g.QuestionText == QuestionText && g.isDelete == false
                                              select g.QuestionText).FirstOrDefault();
                    if (QuestionNoVal != null)
                    {
                        result = "Question No Already exists";
                    }
                    else if (QuestionTextVal != null)
                    {
                        result = "Question Already exists";
                    }
                    else
                    {
                        tblQuestion objtbl = new tblQuestion();
                        objtbl.QuestionNo = QuestionNo;
                        objtbl.QuestionText = QuestionText;
                        objtbl.QuestionType = QuestionType;
                        objtbl.CreatedBy = UserVM._UserID;
                            objtbl.CreatedDate = DateTime.Now.ToUniversalTime();
                        objtbl.isDelete = false;                      
                            objtbl.Inputdata = Inputdata;
                        db.tblQuestions.Add(objtbl);
                        //db.Entry(objtbl).State = EntityState.Added;
                        db.SaveChanges();
                        result = "success";

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("QuestionnaireBL", "AddQuestionnair", err);
                result = "error";
            }
            return result;
        }
        public string UpdateQuestionnair(string QuestionNo, string QuestionText, string QuestionType, int id,bool Inputdata)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string QuestionNoVal = (from g in db.tblQuestions
                                            where g.QuestionNo == QuestionNo && g.id != id && g.isDelete == false
                                            select g.QuestionNo).FirstOrDefault();
                    string QuestionTextVal = (from g in db.tblQuestions
                                              where g.QuestionText == QuestionText && g.id != id && g.isDelete == false
                                              select g.QuestionText).FirstOrDefault();
                    if (QuestionNoVal != null)
                    {
                        result = "QuestionNo Already exists";
                    }
                    else if (QuestionTextVal != null)
                    {
                        result = "Question Already exists";
                    }
                    else
                    {
                        var res = db.tblQuestions.SingleOrDefault(b => b.id == id);
                        if (res != null)
                        {
                            res.QuestionNo = QuestionNo;
                            res.QuestionText = QuestionText;
                            res.QuestionType = QuestionType;
                            res.ModifiedBy = UserVM._UserID;
                            res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                            res.Inputdata = Inputdata;
                            db.SaveChanges();
                            result = "success";
                        }
                        else
                        {
                            result = "Data not found";
                        }

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("QuestionnaireBL", "UpdateQuestionnair", err);
                result = "error";
            }
            return result;
        }
        public QuestionnaireListVM LoadAllQuestionnairs()
        {
            QuestionnaireListVM objResult = new QuestionnaireListVM();
            List<QuestionnaireVM> objData = new List<QuestionnaireVM>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objData = (from g in db.tblQuestions
                               where g.isDelete == false
                               select new QuestionnaireVM
                               {
                                   id = g.id,
                                   CreatedBy = g.CreatedBy,
                                   CreatedDate = g.CreatedDate,
                                   ModifiedBy = g.ModifiedBy,
                                   ModifiedDate = g.ModifiedDate,
                                   QuestionNo = g.QuestionNo,
                                   QuestionText = g.QuestionText,
                                   QuestionType = g.QuestionType,
                                   Inputdata =g.Inputdata,
                                   
                               }).ToList();

                    objResult.QuestionnaireList = objData;
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("QuestionnaireBL", "LoadAllQuestionnairs", err);

            }
            return objResult;
        }

        public QuestionnaireVM LoadSingleQuestionnaire(int id)
        {
            QuestionnaireVM objResult = new QuestionnaireVM();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblQuestions
                                 where g.id == id
                                 select new QuestionnaireVM
                                 {
                                     id = g.id,
                                     CreatedBy = g.CreatedBy,
                                     CreatedDate = g.CreatedDate,
                                     ModifiedBy = g.ModifiedBy,
                                     ModifiedDate = g.ModifiedDate,
                                     QuestionNo = g.QuestionNo,
                                     QuestionText = g.QuestionText,
                                     QuestionType = g.QuestionType,
                                     Inputdata = g.Inputdata
                }).FirstOrDefault();


                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("QuestionnaireBL", "LoadSingleQuestionnaire", err);

            }
            return objResult;
        }

        public string DeleteQuestionnaire(int id)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    var res = db.tblQuestions.SingleOrDefault(b => b.id == id);
                    if (res != null)
                    {
                        res.isDelete = true;
                        res.ModifiedBy = UserVM._UserID;
                        res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                        db.SaveChanges();
                    }
                    result = "success";
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("QuestionnaireBL", "DeleteQuestionnaire", err);
                result = "error";
            }
            return result;
        }
    }
}