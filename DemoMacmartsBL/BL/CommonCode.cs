﻿using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace DemoMacmartsBL.BL
{
   public class CommonCode
    {

        public List<CurrencyVM> getCurrency()
        {
            List<CurrencyVM> obj = new List<CurrencyVM>();
            using (var db = new Demo_Macmarts_DemoEntities())
            {
                obj = (from g in db.tblCurrencyCodes
                       where g.ExchangeRate != null
                       select new CurrencyVM
                       {
                           CurrencyCode = g.Currency,
                           Country = g.Country
                       }).ToList();
            }
            return obj;
        }

        public void InsertIntoUserClientVendorMapping(long UserId, long ClientVendorId, string UserRole, string ClientOrVendor)
        {
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    tblUserClientVendorRoleMapping obj = new tblUserClientVendorRoleMapping();
                    obj.userid = UserId;
                    obj.UserRole = UserRole;
                    if (ClientOrVendor == "V")
                    {
                        if (UserRole == "A")
                        {
                            obj.RoleName = "Administrator";
                        }
                        else if (UserRole == "V")
                        {
                            obj.RoleName = "Supplier User";
                        }
                    }
                    else if (ClientOrVendor == "C")
                    {
                        if (UserRole == "L")
                        {
                            obj.RoleName = "Administrator";
                        }
                        else if (UserRole == "E")
                        {
                            obj.RoleName = "General User";
                        }
                        else if (UserRole == "T")
                        {
                            obj.RoleName = "Team Leader";
                        }
                        else if (UserRole == "M")
                        {
                            obj.RoleName = "Manager";
                        }
                        else if (UserRole == "P")
                        {
                            obj.RoleName = "PEx Admin";
                        }
                        else if (UserRole == "FA")
                        {
                            obj.RoleName = "Finance Admin";
                        }
                        else if (UserRole == "FT")
                        {
                            obj.RoleName = "Finance Team Member";
                        }
                    }

                    obj.isActive = true;
                    obj.ClientOrVendor = ClientOrVendor;
                    obj.ClientVendorId = ClientVendorId;
                    obj.CreatedBy = UserVM._UserID;
                    obj.CreatedDate = DateTime.Now.ToUniversalTime();
                    db.tblUserClientVendorRoleMappings.Add(obj);
                    db.SaveChanges();
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("CommonCode", "InsertIntoUserClientVendorMapping", err);

            }
        }
        public void UpdateUserClientVendorMapping(long UserId, long ClientVendorId, string UserRole, string ClientOrVendor, bool isActive)
        {
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    var res = db.tblUserClientVendorRoleMappings.SingleOrDefault(b => b.userid == UserId && b.ClientOrVendor == ClientOrVendor && b.ClientVendorId == ClientVendorId);
                    if (res != null)
                    {
                        res.UserRole = UserRole;
                        res.ClientVendorId = ClientVendorId;
                        res.isActive = isActive;
                        res.ModifiedBy = UserVM._UserID;
                        res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("CommonCode", "InsertIntoUserClientVendorMapping", err);

            }
        }
    }
}
