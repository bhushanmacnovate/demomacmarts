﻿using DemoMacmartsBL.ViewModel;
using DemoMacmartsCommon;
using DemoMacmartsDL;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;


namespace DemoMacmartsBL.BL.Supplier
{
    public class SupplierBL
    {
        public string AddSupplier(SuppliersVM obj)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string SessionId = Convert.ToString(HttpContext.Current.Session["SessionId"]);
                    int CurrencyCount = db.tblVendorCurrencyMapping_Temp.Count(x => x.SessionId == SessionId);

                    string Email1Val = (from g in db.tblVendorMasters
                                        join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                        where (v.Email1 == obj.Email1 || v.Email2 == obj.Email1) 
                                        select v.Email1).FirstOrDefault();

                    if (Email1Val != null && Email1Val != "" && obj.Email1 != "")
                    {
                        result = "Email Id Already exists";
                    }
                    string SupplierId = (from g in db.tblVendorMasters
                                         join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                         where v.SupplierId == obj.SupplierId
                                         select v.SupplierId).FirstOrDefault();


                    if (SupplierId != "" && SupplierId != null)
                    {
                        result = "Supplier Id Already exists";
                    }
                    else if (CurrencyCount == 0)
                    {
                        result = "Currency not added,Please click on add currency button";
                    }
                    else
                    {
                        var company = db.tblVendorMasters.Where(x => x.Company == obj.CompanyName1).FirstOrDefault();
                        long vendorid = 0;
                        if (company == null)
                        {
                            tblVendorMaster objtbl = new tblVendorMaster();
                            //objtbl.SupplierId = obj.SupplierId;
                            objtbl.Company = obj.CompanyName1;
                            objtbl.CompanyName2 = obj.CompanyName2;
                            objtbl.AddressLine1 = obj.AddressLine1;
                            objtbl.AddressLine2 = obj.AddressLine2;
                            objtbl.PinCode = obj.PinCode;
                            objtbl.CountryName = obj.CountryName;
                            objtbl.StateName = obj.StateName;
                            objtbl.CityName = obj.CityName;
                            objtbl.ContactPerson = obj.ContactPerson;
                            objtbl.Phone1 = obj.Phone1;
                            objtbl.Phone2 = obj.Phone2;
                            objtbl.Email1 = obj.Email1;


                            //objtbl.BankName = obj.BankName;
                            //objtbl.BankBranch = obj.BankBranch;
                            //objtbl.SwiftCode = obj.SwiftCode;
                            //objtbl.AccountNumber = obj.AccountNumber;
                            //objtbl.IBN = obj.IBN;
                            //objtbl.Logo = obj.Logo;

                            //objtbl.AuditReportFile = obj.AuditReportFile;
                            //objtbl.AuditUploadDate= obj.AuditUploadDate;
                            //objtbl.AuditDueDate= obj.AuditDueDate;

                            //objtbl.NDAFile = obj.NDAFile;
                            //objtbl.NDAUploadDate = obj.NDAUploadDate;
                            //objtbl.NDADueDate = obj.NDADueDate;

                            //objtbl.Currency = obj.Currency;

                            objtbl.CreatedBy = UserVM._UserID;
                            objtbl.CreatedDate = DateTime.Now.ToUniversalTime();
                            objtbl.isActive = Convert.ToBoolean(obj.isActive);
                            objtbl.isDelete = false;

                            db.tblVendorMasters.Add(objtbl);
                            db.SaveChanges();
                            vendorid = objtbl.VendorId;
                        }
                        else
                        {
                            vendorid = company.VendorId;
                        }

                        tblVendorClientMapping objvc = new tblVendorClientMapping();
                       // objvc.ClientId = ClientId;
                        objvc.VendorId = vendorid;
                        objvc.SupplierId = obj.SupplierId;
                        objvc.Phone1 = obj.Phone1;
                        objvc.Phone2 = obj.Phone2;
                        objvc.ContactPerson = obj.ContactPerson;
                        objvc.Email1 = obj.Email1;
                        objvc.isActive = obj.isActive;
                        db.tblVendorClientMappings.Add(objvc);
                        db.SaveChanges();

                        List<string> CurrencyList = db.tblVendorCurrencyMapping_Temp.Where(x => x.SessionId == SessionId).Select(x => x.Currency).ToList();
                        foreach (var c in CurrencyList)
                        {
                            tblVendorCurrencyMapping objc = new tblVendorCurrencyMapping();
                            objc.Currency = c.ToString();
                            objc.vendorid = vendorid;
                            db.tblVendorCurrencyMappings.Add(objc);
                            db.SaveChanges();
                        }
                    }
                    result = "success";
                }
            }

            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "AddSupplier", err);
                result = "error";
            }
            return result;
        }
        public string AddSupplierCurrency(long VendorId, string Currency)
        {
            string Result = "OK";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    if (VendorId != 0)
                    {
                        int Count = db.tblVendorCurrencyMappings.Where(x => x.vendorid == VendorId && x.Currency == Currency).Count();
                        if (Count > 0)
                        {
                            Result = "Currency already added.Please add different currency.";
                        }
                        else
                        {
                            tblVendorCurrencyMapping objtbl = new tblVendorCurrencyMapping();
                            objtbl.vendorid = VendorId;
                            objtbl.Currency = Currency;
                            db.tblVendorCurrencyMappings.Add(objtbl);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        string SessionId = Convert.ToString(HttpContext.Current.Session["SessionId"]);

                        int Count = db.tblVendorCurrencyMapping_Temp.Where(x => x.SessionId == SessionId && x.Currency == Currency).Count();
                        if (Count > 0)
                        {
                            Result = "Currency already added.Please add different currency.";
                        }
                        else
                        {
                            tblVendorCurrencyMapping_Temp objtbl = new tblVendorCurrencyMapping_Temp();
                            objtbl.SessionId = SessionId;
                            objtbl.Currency = Currency;
                            db.tblVendorCurrencyMapping_Temp.Add(objtbl);
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                Result = "Error";
                CommonMethods.LogError("SuppliersBL", "AddSupplierCurrency", err);
            }
            return Result;
        }

        public List<string> LoadSupplierCurrency(long VendorId)
        {
            List<string> CurrencyList = new List<string>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    if (VendorId != 0)
                    {
                        CurrencyList = db.tblVendorCurrencyMappings.Where(x => x.vendorid == VendorId).Select(x => x.Currency).ToList();
                    }
                    else
                    {
                        string SessionId = Convert.ToString(HttpContext.Current.Session["SessionId"]);
                        CurrencyList = db.tblVendorCurrencyMapping_Temp.Where(x => x.SessionId == SessionId).Select(x => x.Currency).ToList();
                    }
                }

            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "LoadSupplierCurrency", err);
            }

            return CurrencyList;
        }

        public List<SuppliersVM> LoadAllSuppliers()
        {
            List<SuppliersVM> objResult = new List<SuppliersVM>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblVendorMasters
                                 join c in db.tblVendorClientMappings on g.VendorId equals c.VendorId
                                 
                                 
                                 select new SuppliersVM
                                 {
                                     id = g.VendorId,
                                     SupplierId = c.SupplierId,
                                     CompanyName1 = g.Company,
                                     CompanyName2 = g.CompanyName2,
                                     AddressLine1 = g.AddressLine1,
                                     AddressLine2 = g.AddressLine2,
                                     PinCode = g.PinCode,
                                     CountryName = g.CountryName,
                                     
                                     StateName = g.StateName,
                                     CityName = g.CityName,
                                     Phone1 = c.Phone1,
                                     Phone2 = c.Phone2,
                                     ContactPerson = c.ContactPerson,
                                     Email1 = c.Email1,
                                     //BankName=g.BankName,
                                     //BankBranch=g.BankBranch,
                                     //SwiftCode=g.SwiftCode,
                                     //AccountNumber=g.AccountNumber,
                                     //IBN=g.IBN,
                                     //Logo=g.Logo,
                                     //AuditReportFile=g.AuditReportFile,
                                     //AuditUploadDate=g.AuditUploadDate,
                                     //AuditDueDate=g.AuditDueDate,
                                     //NDAFile=g.NDAFile,
                                     //NDAUploadDate=g.NDAUploadDate,
                                     //NDADueDate=g.NDADueDate,
                                     isActive = c.isActive
                                 }).ToList();
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "LoadAllSuppliers", err);

            }
            return objResult;
        }

        public string UpdateSupplier(SuppliersVM obj)
        {
            string result = "";
            int OpenProjectCount = 0;
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string Email1Val = (from g in db.tblVendorMasters
                                        join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                        where (v.Email1 == obj.Email1 || v.Email2 == obj.Email1) &&  g.VendorId != obj.id
                                        select v.Email1).FirstOrDefault();

                    string SupplierId = (from g in db.tblVendorMasters
                                         join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                         where (v.SupplierId == obj.SupplierId) && g.VendorId != obj.id
                                         select v.SupplierId).FirstOrDefault();

                    bool isValid = true;
                   // OpenProjectCount = getOpenProjects(obj.id);
                    if (obj.isActive == false)
                    {
                        if (OpenProjectCount > 0)
                        {
                            isValid = false;
                        }
                    }

                    if (Email1Val != null && obj.Email1 != "")
                    {
                        result = "Email Id Already exists";
                    }
                    else if (SupplierId != "" && SupplierId != null)
                    {
                        result = "Supplier Id Already exists";
                    }
                    else if (isValid == false)
                    {
                        result = "This supplier has " + OpenProjectCount + " open orders, cannot deactivate. Close the orders first and then proceed to deactivation";
                    }
                    else
                    {
                        //var res = db.tblVendorMasters.SingleOrDefault(b => b.VendorId == obj.id);
                        var res = (from g in db.tblVendorMasters
                                   join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                  
                                   select g).FirstOrDefault();

                        var res1 = (from g in db.tblVendorMasters
                                    join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                    where  g.VendorId == obj.id
                                    select v).FirstOrDefault();
                        if (res != null)
                        {
                            //res.SupplierId = obj.SupplierId;
                            res.Company = obj.CompanyName1;
                            res.CompanyName2 = obj.CompanyName2;
                            res.AddressLine1 = obj.AddressLine1;
                            res.AddressLine2 = obj.AddressLine2;
                            res.PinCode = obj.PinCode;
                            res.CountryName = obj.CountryName;
                            res.StateName = obj.StateName;
                            res.CityName = obj.CityName;
                            res.Phone1 = obj.Phone1;
                            res.Phone2 = obj.Phone2;
                            res.ContactPerson = obj.ContactPerson;
                            res.Email1 = obj.Email1;

                            //res.BankName = obj.BankName;
                            //res.BankBranch = obj.BankBranch;
                            //res.SwiftCode = obj.SwiftCode;
                            //res.AccountNumber = obj.AccountNumber;
                            //res.IBN = obj.IBN;
                            //res.Logo = obj.Logo;

                            //res.AuditReportFile = obj.AuditReportFile;
                            //res.AuditUploadDate = obj.AuditUploadDate;
                            //res.AuditDueDate = obj.AuditDueDate;

                            ////res.Currency = obj.Currency;

                            //res.NDAFile = obj.NDAFile;
                            //res.NDAUploadDate = obj.NDAUploadDate;
                            //res.NDADueDate = obj.NDADueDate;

                            res.isActive = Convert.ToBoolean(obj.isActive);

                            res.ModifiedBy = UserVM._UserID;
                            res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                            db.SaveChanges();


                            if (obj.isActive == false)
                            {
                                DeactivateAllSupplierUser(obj.id);
                            }

                            result = "success";
                        }
                        if (res1 != null)
                        {
                            res1.Phone1 = obj.Phone1;
                            res1.Phone2 = obj.Phone2;
                            res1.ContactPerson = obj.ContactPerson;
                            res1.Email1 = obj.Email1;
                            
                            res1.isActive = obj.isActive;
                            db.SaveChanges();
                            result = "success";


                        }
                        else
                        {
                            result = "Data not found";
                        }

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "UpdateSupplier", err);
                result = "error";
            }
            return result;
        }
        public void DeactivateAllSupplierUser(long VendorId)
        {
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    List<long> UserIds = new List<long>();
                    UserIds = (from g in db.tblUsers
                               join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                               where u.ClientVendorId == VendorId && u.ClientOrVendor == "V"
                               select g.UserId).Distinct().ToList();

                    db.tblUsers
                   .Where(x => UserIds.Contains(x.UserId))
                   .ToList()
                   .ForEach(a => a.isActive = false);
                    db.SaveChanges();
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "DeactivateAllSupplierUser", err);
            }
        }

        //public int getOpenProjects(long VendorId)
        //{
        //    int OpenProjects = 0;
        //    try
        //    {
        //        using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
        //        {
        //            OpenProjects = db.tblProjectRequisitions.Count(x => x.Status != "C" && x.SelectedVendorId == VendorId);
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        CommonMethods.LogError("SuppliersBL", "getOpenProjects", err);
        //    }
        //    return OpenProjects;
        //}

        public SuppliersVM LoadSingleSupplier(long id)
        {
            SuppliersVM objResult = new SuppliersVM();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {

                    List<string> CurrencyList = db.tblVendorCurrencyMappings.Where(x => x.vendorid == id).Select(x => x.Currency).ToList();

                    objResult = (from g in db.tblVendorMasters
                                 join v in db.tblVendorClientMappings on g.VendorId equals v.VendorId
                                 where g.VendorId == id 
                                 select new SuppliersVM
                                 {
                                     id = g.VendorId,
                                     SupplierId = v.SupplierId,
                                     CompanyName1 = g.Company,
                                     CompanyName2 = g.CompanyName2,
                                     AddressLine1 = g.AddressLine1,
                                     AddressLine2 = g.AddressLine2,
                                     PinCode = g.PinCode,
                                     CountryName = g.CountryName,
                                     StateName = g.StateName,
                                     CityName = g.CityName,
                                     Phone1 = g.Phone1,
                                     Phone2 = g.Phone2,
                                     ContactPerson = g.ContactPerson,
                                     Email1 = g.Email1,
                                     //BankName = g.BankName,
                                     //BankBranch = g.BankBranch,
                                     //SwiftCode = g.SwiftCode,
                                     //AccountNumber = g.AccountNumber,
                                     //IBN = g.IBN,
                                     //Logo = g.Logo,
                                     //AuditReportFile = g.AuditReportFile,
                                     //AuditUploadDate = g.AuditUploadDate,
                                     //AuditDueDate = g.AuditDueDate,
                                     //NDAFile = g.NDAFile,
                                     //NDAUploadDate = g.NDAUploadDate,
                                     //NDADueDate = g.NDADueDate,
                                     isActive = g.isActive,
                                     Currency = g.Currency,
                                     CurrencyList = CurrencyList
                                 }).FirstOrDefault();


                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "LoadSingleSupplier", err);

            }
            return objResult;
        }

        public string DeleteSupplierCurrency(long VendorId, string Currency)
        {
            string Result = "OK";

            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    if (VendorId != 0)
                    {
                       // int Count = db.tblVendorRequisitionMappings.Where(x => x.VendorId == VendorId && x.SupplierCurrency == Currency).Count();
                        //if (Count > 0)
                        //{
                        //    Result = "Selected currency already is in use with some of the requisitions. Please contact administrator.";
                        //}
                        //else
                        //{
                           int Count = db.tblVendorCurrencyMappings.Count(x => x.vendorid == VendorId);
                            if (Count > 1)
                            {
                                db.tblVendorCurrencyMappings.RemoveRange(db.tblVendorCurrencyMappings.Where(x => x.vendorid == VendorId && x.Currency == Currency));
                                db.SaveChanges();
                            }
                            else
                            {
                                Result = "You can not delete this currency,supplier must have atleast one currency.";
                            }
                        }
                    
                    else
                    {
                        string SessionId = Convert.ToString(HttpContext.Current.Session["SessionId"]);
                        db.tblVendorCurrencyMapping_Temp.RemoveRange(db.tblVendorCurrencyMapping_Temp.Where(x => x.SessionId == SessionId && x.Currency == Currency));
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception err)
            {
                Result = "Error";
                CommonMethods.LogError("SuppliersBL", "DeleteSupplierCurrency", err);
            }

            return Result;
        }

        public List<SupplierUsersVM> LoadAllSupplierUsers()
        {
            List<SupplierUsersVM> objResult = new List<SupplierUsersVM>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblUsers
                                 join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                 join v in db.tblVendorMasters on u.ClientVendorId equals v.VendorId
                                 join c in db.tblVendorClientMappings on u.ClientVendorId equals c.VendorId
                                 where  u.ClientOrVendor == "V" && g.isDelete == false
                                 select new SupplierUsersVM
                                 {
                                     UserId = g.UserId,
                                     VendorId = u.ClientVendorId,
                                     SupplierId = c.SupplierId,
                                     VendorName = c.SupplierId + "-" + v.Company + " " + v.CompanyName2,
                                     FirstName = g.FirstName,
                                     LastName = g.LastName,
                                     Email1 = g.EmailId,
                                     //Email2 = g.Email2,
                                     //Phone1 = g.Phone1,
                                     //Phone2 = g.Phone2,
                                     IsEmailVerified = g.IsEmailVerified,
                                     SupplierRole = u.UserRole,
                                     IsActive = g.isActive
                                 }).Distinct().ToList();
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "LoadAllSupplierUsers", err);

            }
            return objResult;
        }

        public SupplierUsersVM LoadSingleSupplierUser(long Userid)
        {
            SupplierUsersVM objResult = new SupplierUsersVM();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    objResult = (from g in db.tblUsers
                                 join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                 where g.UserId == Userid
                                 select new SupplierUsersVM
                                 {
                                     UserId = g.UserId,
                                     VendorId = u.ClientVendorId,
                                     FirstName = g.FirstName,
                                     LastName = g.LastName,
                                     Email1 = g.EmailId,
                                     //Email2=g.Email2,
                                     //Phone1=g.Phone1,
                                     //Phone2=g.Phone2,       
                                     Password = g.Password,
                                     ConfirmPassword = g.Password,
                                     SupplierRole = u.UserRole,
                                     IsActive = g.isActive
                                 }).FirstOrDefault();


                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "LoadSingleSupplierUser", err);

            }
            return objResult;
        }

        public List<SuppliersVM> GetAllSupplierList()
        {
            List<SuppliersVM> model = new List<SuppliersVM>();
            using (Demo_Macmarts_DemoEntities _db = new Demo_Macmarts_DemoEntities())
            {
                model = (from v in _db.tblVendorMasters
                         join m in _db.tblVendorClientMappings on v.VendorId equals m.VendorId
                         where v.isActive == true
                         select new SuppliersVM
                         {
                             id = v.VendorId,
                             SupplierName = m.SupplierId + " - " + v.Company + " " + v.CompanyName2,
                         }).ToList();
                return model;
            }
        }

        public List<GenericStringDropdownVM> getSupplierRoles(long Id)
        {
            List<GenericStringDropdownVM> objResult = new List<GenericStringDropdownVM>();
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    int AdminCount = (from g in db.tblUsers
                                      join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                      where u.ClientOrVendor == "V" && u.UserRole == "A" && u.isActive == true && g.isActive == true && g.isDelete == false
                                      select g.UserId).Count();

                    //db.Users.Count(x => x.VendorId == Id && x.UserType == "V" && x.Role == "A");

                    if (AdminCount > 0)
                    {
                        GenericStringDropdownVM obj = new GenericStringDropdownVM();
                        obj.Text = "General";
                        obj.Value = "V";
                        objResult.Add(obj);
                    }

                    GenericStringDropdownVM obj1 = new GenericStringDropdownVM();
                    obj1.Text = "Admin";
                    obj1.Value = "A";
                    objResult.Add(obj1);
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "getSupplierRole", err);
            }
            return objResult;
        }

        public string AddSupplierUser(SupplierUsersVM obj)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string Email1Val = (from g in db.tblUsers
                                        where (g.EmailId == obj.Email1 || g.AltEmailId == obj.Email1) && g.UserId != obj.UserId
                                        select g.EmailId).FirstOrDefault();


                    if (Email1Val != null)
                    {
                        result = "Email Id Already exists";
                    }
                    else
                    {
                        tblUser objtbl = new tblUser();
                        objtbl.FirstName = obj.FirstName;
                        objtbl.LastName = obj.LastName;
                        objtbl.EmailId = obj.Email1;
                        //objtbl.Email2 = obj.Email2;
                        //objtbl.Phone1 = obj.Phone1;
                        //objtbl.Phone2 = obj.Phone2;
                        objtbl.Password = obj.Password;
                        //objtbl.UserType = "V";
                        //objtbl.Role = obj.SupplierRole;
                        //objtbl.VendorId = obj.VendorId;
                        objtbl.isActive = obj.IsActive;
                        objtbl.isDelete = false;
                        objtbl.UserPic = "default.png";
                        objtbl.IsEmailVerified = true;
                        //objtbl.IsOffice365Login = false;

                        objtbl.CreatedBy = UserVM._UserID;
                        objtbl.CreatedDate = DateTime.Now.ToUniversalTime();

                        db.tblUsers.Add(objtbl);
                        db.SaveChanges();

                        long UserId = objtbl.UserId;

                        CommonCode objcommon = new CommonCode();
                        objcommon.InsertIntoUserClientVendorMapping(UserId, (long)obj.VendorId, obj.SupplierRole, "V");

                        result = "success";
                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "AddSupplierUser", err);
                result = "error";
            }
            return result;
        }

        public string UpdateSupplierUser(SupplierUsersVM obj)
        {
            string result = "";
            try
            {
                using (Demo_Macmarts_DemoEntities db = new Demo_Macmarts_DemoEntities())
                {
                    string Email1Val = (from g in db.tblUsers
                                        where (g.EmailId == obj.Email1 || g.AltEmailId == obj.Email1) && g.UserId != obj.UserId
                                        select g.EmailId).FirstOrDefault();

                    long? VendorId = (from g in db.tblUsers
                                      join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                      where g.UserId == obj.UserId && u.ClientOrVendor == "V"
                                      select u.ClientVendorId).FirstOrDefault();

                    int TotalAdmin = (from g in db.tblUsers
                                      join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                      where u.UserRole == "A" && u.ClientOrVendor == "V" && u.ClientVendorId == VendorId
                                      select g.UserId).Count();

                    //db.Users.Count(x => x.VendorId == VendorId && x.Role == "A");

                    string UserRole = (from g in db.tblUsers
                                       join u in db.tblUserClientVendorRoleMappings on g.UserId equals u.userid
                                       where u.ClientOrVendor == "V" && g.UserId == obj.UserId
                                       select u.UserRole).FirstOrDefault();
                    //db.Users.Where(x => x.UserId == obj.UserId).Select(x => x.Role).FirstOrDefault();

                    bool isValid = true;

                    bool isValid1 = true;
                    bool isVendorActive = (from g in db.tblVendorMasters
                                           where g.VendorId == obj.VendorId
                                           select g.isActive).FirstOrDefault();

                    if (TotalAdmin > 1)
                    {
                        isValid = true;
                    }
                    else if (TotalAdmin <= 1)
                    {
                        if (UserRole == "A" && obj.SupplierRole == "V")
                        {
                            isValid = false;
                        }
                    }

                    //if(isVendorActive==false)
                    //{
                    //    isValid1 = false;
                    //}

                    if (Email1Val != null)
                    {
                        result = "Email Id Already exists";
                    }
                    else if (isValid == false)
                    {
                        result = "You cannot change role of selected Supplier.At least one user should be an administrator";
                    }
                    //else if (isValid1 == false)
                    //{
                    //    result = "You cannot update user details,Supplier is InActive.";
                    //}
                    else
                    {
                        var res = db.tblUsers.SingleOrDefault(b => b.UserId == obj.UserId);
                        if (res != null)
                        {
                            res.FirstName = obj.FirstName;
                            res.LastName = obj.LastName;
                            res.EmailId = obj.Email1;
                            //res.Email2 = obj.Email2;
                            //res.Phone1 = obj.Phone1;
                            //res.Phone2 = obj.Phone2;
                            //res.Role = obj.SupplierRole;
                            //res.VendorId = obj.VendorId;
                            res.isActive = obj.IsActive;
                            res.ModifiedBy = UserVM._UserID;
                            res.ModifiedDate = System.DateTime.Now.ToUniversalTime();
                            db.SaveChanges();

                            CommonCode objcommon = new CommonCode();
                            objcommon.UpdateUserClientVendorMapping(obj.UserId, (long)obj.VendorId, obj.SupplierRole, "V", (bool)obj.IsActive);
                            result = "success";
                        }
                        else
                        {
                            result = "Data not found";
                        }

                    }
                }
            }
            catch (Exception err)
            {
                CommonMethods.LogError("SuppliersBL", "UpdateSupplierUser", err);
                result = "error";
            }
            return result;
        }

    }
}
