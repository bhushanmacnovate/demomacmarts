﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL.ViewModel
{
    
    public class UnitOfMeasureVM
    {
        public int id { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string Description { get; set; }
        public long? CreatedBy { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public DateTime? ModifiedDate { get; set; }
    }

    public class CurrencyVM
    {
        public string CurrencyCode { get; set; }
        public string Country { get; set; }
    }
    public class UOMMainVM
    {
        public UnitOfMeasureVM UOM { get; set; }
        public UOMListVM UOMList { get; set; }
    }

    public class UOMListVM
    {
        public List<UnitOfMeasureVM> UOMlist { get; set; }
    }

}
