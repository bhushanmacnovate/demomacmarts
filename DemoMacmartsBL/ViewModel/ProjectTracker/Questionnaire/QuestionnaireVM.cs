﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL.ViewModel
{
    public class QuestionnaireVM
    {
        public long id { get; set; }
        public int? QuestionId { get; set; }
        public string QuestionNo { get; set; }
        public string QuestionText { get; set; }
        public bool? Answer { get; set; }
        public string QuestionType { get; set; }
        public bool? Notification { get; set; }
        public bool? Inputdata { get; set; }

        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public bool? isActive { get; set; }
        public bool? isDelete { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public long? RequisitionId { get; set; }
        public int? StatusID { get; set; }


    }

    public class QuestionnaireListVM
    {
        public bool? Inputdata { get; set; }
        public List<QuestionnaireVM> QuestionnaireList { get; set; }
    }
    public class QuestionnaireMainVM
    {
        public bool? Inputdata { get; set; }
        public QuestionnaireVM Questionnaire { get; set; }
        public QuestionnaireListVM QuestionnaireList { get; set; }
    }
}
