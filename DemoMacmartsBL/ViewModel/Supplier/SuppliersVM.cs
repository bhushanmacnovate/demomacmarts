﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL.ViewModel
{
    public class SuppliersVM
    {
        public long id { get; set; }
        public string SupplierId { get; set; }
        public string SupplierName { get; set; }
        public string CompanyName1 { get; set; }
        public string CompanyName2 { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public long? CountryId { get; set; }
        public string CountryName { get; set; }
        public string StateName { get; set; }
        public string CityName { get; set; }
        public string PinCode { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string ContactPerson { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Expertise { get; set; }
        public string BankName { get; set; }
        public string BankBranch { get; set; }
        public string SwiftCode { get; set; }
        public string AccountNumber { get; set; }
        public string IBN { get; set; }
        public string Logo { get; set; }
        public bool? isActive { get; set; }
        public bool isDelete { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public List<int> ExpertiseList { get; set; }
       // public FileUploadViewModel FileUpload { get; set; }

        public string AuditReportFile { get; set; }
        public DateTime? AuditUploadDate { get; set; }
        public DateTime? AuditDueDate { get; set; }

        public string NDAFile { get; set; }
        public DateTime? NDAUploadDate { get; set; }
        public DateTime? NDADueDate { get; set; }

        public string Currency { get; set; }
        public string Currency1 { get; set; }
        public string Currency2 { get; set; }
        public string Currency3 { get; set; }
        public string Currency4 { get; set; }
        public string Remark { get; set; }
        public List<string> CurrencyList { get; set; }


    }
}