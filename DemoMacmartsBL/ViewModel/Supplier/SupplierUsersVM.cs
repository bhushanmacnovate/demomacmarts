﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL.ViewModel
{
    public class SupplierUsersVM
    {
        public long UserId { get; set; }
        public long? VendorId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email1 { get; set; }
        public string Email2 { get; set; }
        public string Phone1 { get; set; }
        public string Phone2 { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string VendorName { get; set; }
        public bool IsEmailVerified { get; set; }
        public string SupplierId { get; set; }
        public string SupplierRole { get; set; }
        public bool? IsActive { get; set; }
    }
}