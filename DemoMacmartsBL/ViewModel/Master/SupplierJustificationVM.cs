﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DemoMacmartsBL.ViewModel
{
    public class SupplierJustificationVM
    {
        public long id { get; set; }
        public string SupplierJustification { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ModifiedBy { get; set; }

    }

    public class SupplierJustificationListVM
    {
        public List<SupplierJustificationVM> SupplierJustificationList { get; set; }
    }
    public class SupplierJustificationMainVM
    {
        public SupplierJustificationVM SupplierJustification { get; set; }
        public SupplierJustificationListVM JustificationList { get; set; }
    }
}
